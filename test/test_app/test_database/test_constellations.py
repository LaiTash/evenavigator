from pony.orm import db_session

from app.database.constellations import Constellations


def test_insert_from_esi(db):
    # Needs an existing region
    with db_session:
        db.Region(id=0, name='0')

    fake_data = dict(
        constellation_id=0, name='0', region_id=0
    )

    Constellations(db).insert_from_esi(fake_data)

    with db_session:
        assert db.Constellation.get(id=0).region.name=='0'
