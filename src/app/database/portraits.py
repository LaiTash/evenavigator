from pathlib import Path
from datetime import datetime

from pony.orm import db_session

from app.database.entity import Entity

from typing import Optional, Set, List


class Portraits(Entity):
    _entity_name = 'Portrait'

    @db_session
    def character_records(self, character_id: int) -> List:
        return list(self.entity.select(lambda r: r.character_id==character_id))

    @db_session
    def character_keys(self, character_id: int) -> Set[str]:
        """ Return character portrait keys """
        return set(
            record.key for record in
            self.entity.select(lambda r: r.character_id==character_id)
        )

    @db_session
    def update_from_esi(self, character_id: int, data: dict):
        """ Update records for a specified character from esi model

        :param character_id: character id
        :param data: swagger model as dict
        :return: list of downloaded files to be deleted
        """
        keys = self.character_keys(character_id)
        new_records = {k: v for k, v in data.items() if k not in keys}
        records_to_modify = {k: v for k, v in data.items() if k in keys}
        records_to_delete = keys.difference(data.keys())

        for key, url in new_records.items():
            self.entity(character_id=character_id, key=key, url=url)

        for key, url in records_to_modify.items():
            record = self.entity.get(character_id=character_id, key=key)
            record.url = url

        for key in records_to_delete:
            record = self.entity.get(character_id=character_id, key=key)
            record.delete()
