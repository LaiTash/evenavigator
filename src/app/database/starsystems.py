from typing import Any

from pony.orm import db_session
from pyswagger.io import Response
from pyswagger.primitives import Model

from app.database.entity import EsiMassInsertableEntity


class StarSystems(EsiMassInsertableEntity):
    _entity_name = 'StarSystem'

    @db_session
    def insert_from_esi(self, data: dict) -> None:
        self.entity(
            id=data['system_id'],
            constellation=data['constellation_id'],
            security_status=data['security_status'],
            name=data['name']
        )
