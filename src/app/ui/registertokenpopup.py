from pathlib import Path
from threading import Thread
from typing import Optional

from kivy.lang import Builder
from kivy.uix.popup import Popup

from esi.esifacade import EsiFacade
from esi.ownership import TokenOwnersContainer
from esi.register import create_registrator_server
from esi.token import TokensContainer
import webbrowser
from . import UsesEnvironment

class RegisterTokenPopup(Popup, UsesEnvironment):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self._server_thread: Optional[Thread] = None
        self._esi = self.environment.esi
        self._auth_uri = self._esi.get_auth_uri()
        self._tokens_container = self.environment.tokens
        self._owners_container = self.environment.owners


    def on_open(self):
        self._start()

    def on_dismiss(self):
        self._shutdown_server()

    def _start(self):
        security = self._esi.create_security()
        self._server = create_registrator_server(
            security, self._tokens_container, self._owners_container,
            shutdown_callback=self.dismiss
        )
        self._start_server()
        webbrowser.open(self._auth_uri)

    def _shutdown_server(self):
        if self._server_thread is None:
            return

        thread = self._server_thread
        self._server_thread = None
        self._server.shutdown()
        thread.join()


    def _start_server(self):
        assert self._server_thread is None
        self._server_thread = Thread(target=self._server.serve_forever)
        self._server_thread.setDaemon(True)
        self._server_thread.start()


_path = Path(__file__).with_suffix('.kv')
Builder.load_file(str(_path))
