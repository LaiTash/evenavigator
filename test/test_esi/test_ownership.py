from copy import copy

from esi.ownership import TokenOwner
import pytest


@pytest.fixture
def owner():
    owner_character_id = 0
    owner_character_name = 'Lai Brinalle'
    owner_hash = '0'

    return TokenOwner(owner_character_id, owner_character_name, owner_hash)


def test_token_owner_initialize():
    # Given some owner values
    owner_character_id = 0
    owner_character_name = 'Lai Brinalle'
    owner_hash = '0'

    # When a new owner is defined with those values
    owner = TokenOwner(owner_character_id, owner_character_name, owner_hash)

    # The resulting object should reflect them
    assert owner.character_id == 0
    assert owner.character_name == 'Lai Brinalle'
    assert owner.owner_hash == '0'


def test_eq(owner):
    # Two TokenOwner definitions are equal if their fields are equal
    # even if they are not the same object
    assert owner == copy(owner)


def test_neq(owner):
    other = copy(owner)
    other.character_name = 'other'
    assert owner != other


def test_neq_with_other(owner):
    # When comparing to object of another class, return False
    other = 0
    assert owner != other