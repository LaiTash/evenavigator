import pytest

from ui.graphics.tools import fit


@pytest.mark.parametrize(
    'rect,to_fit,expected', [
        [(4, 2), (2, 4), (2, 1)],
        [(2, 4), (4, 2), (1, 2)],
        [(2, 2), (4, 4), (4, 4)],
        [(4, 4), (2, 2), (2, 2)],
        [(4, 2), (4, 4), (4, 2)],
        [(2, 4), (4, 4), (2, 4)],
        [(4, 4), (4, 2), (2, 2)],
        [(4, 4), (2, 4), (2, 2)]
    ]
)
def test_fit(rect, to_fit, expected):
    result = fit(rect, to_fit)
    assert result == expected
