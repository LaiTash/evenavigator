import pytest
from kivy.uix.button import Button

from ui.pageview import PageView, PageViewToggleButton


@pytest.fixture
def pageview():
    return PageView()


def test_add_widget(pageview):
    child = Button(text="ok")
    pageview.add_widget(child)

    assert len(pageview.children) == 0
    assert child in pageview.pages


def test_set_page_initial(pageview):
    child = Button(text='ok')
    pageview.add_widget(child)

    child.page_id = 'one'
    pageview.set_page('one')

    assert child in pageview.children
    assert pageview.active_page_id == child.page_id


def test_set_page(pageview):
    child_a = Button(text='ok')
    child_b = Button(text='ok')
    pageview.add_widget(child_a)
    pageview.add_widget(child_b)

    child_a.page_id = 'a'
    child_b.page_id = 'b'

    pageview.set_page('a')
    pageview.set_page('b')

    assert child_a not in pageview.children
    assert child_b in pageview.children
    assert pageview.active_page_id == 'b'


def test_set_page_change_size_pos(pageview):
    child = Button(text='ok')
    pageview.add_widget(child)

    child.page_id = 'child'
    pageview.set_page('child')

    pageview.pos = (10, 10)
    assert child.pos == [10, 10]

    pageview.size = (10, 10)
    assert child.size == [10, 10]


def test_togglebutton(pageview):
    child_a = Button(text='a')
    child_b = Button(text='b')
    pageview.add_widget(child_a)
    pageview.add_widget(child_b)

    child_a.page_id = 'a'
    child_b.page_id = 'b'

    toggle_a = PageViewToggleButton(text='a')
    toggle_b = PageViewToggleButton(text='b')
    toggle_a.tracked_page_id = 'a'
    toggle_b.tracked_page_id = 'b'
    toggle_a.view = pageview
    toggle_b.view = pageview

    toggle_a.state = 'down'
    assert pageview.active_page_id == 'a'

    toggle_b.state = 'down'
    assert pageview.active_page_id == 'b'
    assert toggle_a.state == 'normal'

