import os


class EnvironmentFolders(object):
    """ Configuration folders """
    def __init__(self, base_folder: str) -> None:
        self.__base_folder = os.path.abspath(os.path.expanduser(base_folder))
        os.makedirs(self.base_folder, exist_ok=True)
        os.makedirs(self.data_folder, exist_ok=True)
        os.makedirs(self.config_folder, exist_ok=True)

    @property
    def base_folder(self):
        return self.__base_folder

    @property
    def config_folder(self):
        return os.path.join(self.__base_folder, 'config')

    @property
    def data_folder(self):
        return os.path.join(self.__base_folder, 'data')