from typing import Any, Tuple, Optional

from kivy.graphics.instructions import Canvas
from kivy.properties import ListProperty, ObjectProperty, StringProperty
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.widget import Widget


class PageViewToggleButton(ToggleButton):
    view = ObjectProperty()  # type: PageView
    tracked_page_id = StringProperty()

    def on_view(self, instance: Any, view: 'PageView') -> None:
        view.bind(active_page_id=self.on_page_id)

    def on_page_id(self, instance: Any, page_id: Any) -> None:
        self.state = 'down' if page_id == self.tracked_page_id else 'normal'

    def on_state(self, instance: Any, state: str) -> None:
        if state == 'down' and self.view.active_page_id!=self.tracked_page_id:
            self.view.set_page(self.tracked_page_id)



class PageView(Widget):
    pages = ListProperty()  # type: list
    active_page_id = StringProperty()  # type: str

    def on_pos(self, instance: Any, pos: Tuple[float, float]) -> Any:
        if self.children:
            self.children[0].pos = pos

    def on_size(self, instance: Any, size: Tuple[float, float]) -> None:
        if self.children:
            self.children[0].size = size

    def _add_widget(
            self, widget: Widget, index:int=0, canvas:Optional[Canvas]=None
    ) -> None:
        super().add_widget(widget, index, canvas)

    def add_widget(
            self, widget: Widget, index:int=0, canvas:Optional[Canvas]=None
    ) -> None:
        self.pages.append(widget)

    def set_page(self, id:str) -> None:
        self.clear_widgets()
        for page in self.pages:
            if page.page_id != id:
                continue
            page.pos = self.pos
            page.size = self.size
            self._add_widget(page)
            self.active_page_id = id
            return

        raise KeyError(id)
