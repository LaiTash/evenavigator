from typing import Callable, Tuple, Any

from esipy import App
from pyswagger.io import Request, Response


Operation = Tuple[Request, Response]


class OperationDef(object):
    """

    Defines an unbound (i.e. not bound to any application object yet)
    ESI operation definition.

    """
    def __init__(self, op_id:str, *args:Any, **kwargs:Any) -> None:
        self.id = op_id
        self.args = args
        self.kwargs = kwargs


OperationFactory = Callable[
    [App, OperationDef],
    Tuple[Request, Response]
]


def bind_operation_def(
        app: App, operation_def: OperationDef
) -> Operation:
    """ Bind operation definition to an application """
    return app.op[operation_def.id](*operation_def.args, **operation_def.kwargs)
