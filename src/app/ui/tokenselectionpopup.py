import os
from pathlib import Path
from threading import Thread
from typing import Any, Optional

import requests
from kivy import Logger
from kivy.lang import Builder
from kivy.properties import StringProperty, partial, ObjectProperty, \
    NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup

from app.environment.portraitmanager import CharacterPortraitCache
from esi.esifacade import AsyncEsiFacade
from esi.operations.characters import portrait
from esi.ownership import TokenOwnersContainer
from esi.token import TokensContainer
from simpletools.exception import LogExceptionHandler, \
    format_exception_traceback
from simpletools.futures import unwrap_future
from kivy.clock import Clock
from . import UsesEnvironment


class TokenWidget(BoxLayout, UsesEnvironment):
    token_key = StringProperty()  # type: Optional[str]
    character_name = StringProperty()  # type: Optional[int]
    character_id = NumericProperty()  # type: Optional[int]

    deleted = False

    def __init__(self, **kwargs):
        self.register_event_type('on_select')
        self.register_event_type('on_delete')
        super().__init__(**kwargs)

    def on_select(self):
        pass

    def on_delete(self):
        if not self.deleted:
            self.environment.tokens.delete(self.token_key)
            del self.environment.owners[self.token_key]
            self.deleted = True

    def on_token_key(self, _: Any, token_key: str) -> None:
        owner = self.environment.owners[token_key]
        self.character_name = owner.character_name
        self.character_id = owner.character_id


class TokenSelectionPopup(Popup, UsesEnvironment):
    token_widgets = ObjectProperty()

    def __init__(self, **kwargs: Any) -> None:
        self.register_event_type('on_select')
        self.register_event_type('on_delete')

        super().__init__(**kwargs)

        self.update()

    def on_select(self, token_key: str) -> None:
        pass

    def on_delete(self, token_key: str) -> None:
        pass

    def _on_token_selected(self, instance: TokenWidget) -> None:
        self.dispatch('on_select', instance.token_key)

    def _on_token_deleted(self, instance: TokenWidget) -> None:
        Clock.schedule_once(self.update, .1)
        self.dispatch('on_delete', instance.token_key)

    def update(self, *_):
        self.token_widgets.clear_widgets()

        for token_key in self.environment.tokens:
            self.add_token_widget(token_key)

    def add_token_widget(self, token_key):
        widget = TokenWidget(
            environment=self.environment, token_key=token_key
        )
        widget.bind(on_select=self._on_token_selected)
        widget.bind(on_delete=self._on_token_deleted)
        self.token_widgets.add_widget(widget)


_path = Path(__file__).with_suffix('.kv')
Builder.load_file(str(_path))
