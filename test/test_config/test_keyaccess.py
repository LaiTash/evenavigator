import mock
import pytest
from kivy.properties import NumericProperty

from configwrapper.config import KeyAccessConfigContainer


def test_access_property():

    class Container(KeyAccessConfigContainer):
        answer = NumericProperty(42)

    assert Container.from_defaults().answer == 42


def test_set_property():

    class Container(KeyAccessConfigContainer):
        answer = NumericProperty(42)

    container = Container.from_defaults()
    container.answer = 666

    assert container.answer == 666


def test_access_child():
    class Container(KeyAccessConfigContainer):
        child = KeyAccessConfigContainer

    container = Container.from_defaults()
    assert isinstance(container.child, KeyAccessConfigContainer)


def test_from_json():
    class Devil(KeyAccessConfigContainer):
        answer = NumericProperty(666)

    class God(KeyAccessConfigContainer):
        answer = NumericProperty(42)

    class Universe(KeyAccessConfigContainer):
        devil = Devil
        god = God

    serialized = {
        'god': {
            'answer': 666
        },
        'devil': {
            'answer': 42
        }
    }

    universe = Universe.from_json(serialized)
    assert universe.god.answer == 666
    assert universe.devil.answer == 42


def test_from_json_noallow_unknown():
    class Container(KeyAccessConfigContainer):
        answer = NumericProperty()

    serialized = {
        'answer': 42,
        'bad_answer': 666
    }

    with pytest.raises(ValueError):
        Container.from_json(serialized)


class test_from_json_allow_unknown():
    class Container(KeyAccessConfigContainer):
        answer = NumericProperty()

    serialized = {
        'answer': 42,
        'bad_answer': 666
    }

    container = Container.from_json(serialized, True)
    assert container.answer == 42


def test_inform_on_change_when_item_set():
    class Container(KeyAccessConfigContainer):
        answer = NumericProperty()

    container = Container.from_defaults()
    container.on_change = mock.MagicMock()
    container.answer = 42

    assert container.on_change.called


def test_from_json_load_defaults():
    class Devil(KeyAccessConfigContainer):
        answer = NumericProperty(666)

    class God(KeyAccessConfigContainer):
        answer = NumericProperty(42)

    class Universe(KeyAccessConfigContainer):
        devil = Devil
        god = God

    serialized = {
        'devil': {
            'answer': 42
        }
    }

    universe = Universe.from_json(serialized)
    assert universe.god.answer == 42


def test_to_json():
    class Devil(KeyAccessConfigContainer):
        answer = NumericProperty(666)

    class God(KeyAccessConfigContainer):
        answer = NumericProperty(42)

    class Universe(KeyAccessConfigContainer):
        devil = Devil
        god = God

    expected = {
        'god': {
            'answer': 42
        },
        'devil': {
            'answer': 666
        }
    }

    assert Universe.from_defaults().to_json() == expected


def test_inherit_child_on_change():
    class Devil(KeyAccessConfigContainer):
        answer = NumericProperty(666)

    class God(KeyAccessConfigContainer):
        answer = NumericProperty(42)

    class Universe(KeyAccessConfigContainer):
        devil = Devil
        god = God

    universe = Universe.from_defaults()
    universe.on_change = mock.MagicMock()

    universe.god.answer = 666

    assert universe.on_change.called