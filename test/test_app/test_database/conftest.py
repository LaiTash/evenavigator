import pytest
from pony.orm import *

from app.database import bind_evenavigator_tables


def bind_dummy(db):
    class Dummy(db.Entity):
        id = PrimaryKey(int)
        name = Optional(str, nullable=True)


@pytest.fixture
def db():
    db = Database()
    db.bind('sqlite', ':memory:')
    bind_evenavigator_tables(db)
    bind_dummy(db)
    db.generate_mapping(create_tables=True)
    return db