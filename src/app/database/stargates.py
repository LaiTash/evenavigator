from typing import List, Iterable

from pony.orm import db_session, flush
from .entity import EsiMassInsertableEntity


class Stargates(EsiMassInsertableEntity):
    _entity_name = 'Stargate'

    @db_session
    def insert_from_esi(self, data: dict) -> None:
        """ Insert from esi response data

        .. warning::
            Does not set `.destination_stargate`,
            use :py:method:`update_desinations_from_esi` after all the
            stargates had been inserted
        """
        self.entity(
            id=data['stargate_id'],
            type_id=data['type_id'],
            name=data['name'],
            star_system=data['system_id']
        )

    @db_session
    def update_destinations_from_esi(self, data: dict) -> None:
        destination = data.get('destination')
        if destination:
            dest_id = destination['stargate_id']
            self.get(id=data['stargate_id']).destination_stargate = dest_id

    @db_session
    def mass_insert_from_esi(self, data: Iterable[dict]):
        for item in data:
            self.insert_from_esi(item)
        flush()
        for item in data:
            self.update_destinations_from_esi(item)

