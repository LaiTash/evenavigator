from concurrent.futures import Future
from typing import Optional, Dict, Any

import time
from kivy.properties import NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image
from kivy.clock import Clock

from app.environment.portraitmanager import CharacterPortraitCache
from . import UsesEnvironment


class CharacterPortrait(Image, UsesEnvironment):
    """

    Image widget for showing a character's portrait using
    CharacterPortraitCache

    """

    portraits_cache: CharacterPortraitCache
    character_id = NumericProperty()  # type: Optional[int]

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.portraits_cache = self.environment.portraits_cache
        self.try_update()

    def on_size(self, *_: Any) -> None:
        self.try_update()

    def on_character_id(self, *_: Any) -> None:
        self.try_update()

    def try_update(self) -> None:
        if self.character_id is not None:
            self._update()

    def _update(self) -> None:
        if not self._try_set_best_portrait():
            future = self.portraits_cache.request_portraits(self.character_id)
            future.add_done_callback(self._on_portraits_request_done)

    def _on_portraits_request_done(self, future: Future) -> None:
        if future.exception():
            return

        self._try_set_best_portrait()

    def _try_set_best_portrait(self) -> bool:
        portraits = self.portraits_cache.get_downloaded_portraits(
            self.character_id
        )
        if portraits:
            self.source = self._pick_portrait(portraits)
            # Reload in main thread
            Clock.schedule_once(lambda *_: self.reload(), 0.1)
            return True

        return False

    def _pick_portrait(self, portraits: Dict[int, str]) -> str:
        best_size = min(portraits.keys(),
                        key=lambda size: abs(self.width-size))

        return portraits[best_size]


class PortraitButton(ButtonBehavior, CharacterPortrait):
    pass