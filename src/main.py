import os
import platform
from argparse import ArgumentParser, Namespace

import esi_consts
from app.environment import create_environment, AppEnvironment


def main() -> int:
    datafolder = (
        os.getenv('EVENAVIGATOR_DATA_FOLDER') or get_default_base_path()
    )
    environment = create_environment(datafolder, esi_consts)
    launch_app(environment)
    return 0


def parse_args() -> Namespace:
    arg_parser = ArgumentParser()
    arg_parser.add_argument('--updatesystems', action='store_true')

    return arg_parser.parse_args()


def launch_app(environment: AppEnvironment) -> None:
    from app.app import EveNavigatorApp
    EveNavigatorApp(environment).run()


def get_default_base_path() -> str:
    if platform.system() == 'Windows':
        appdata_path = os.getenv('LOCALAPPDATA')
        if appdata_path is None:
            raise ValueError("Could not get appdata path")
        return os.path.join(
            appdata_path,
            'PyEveMap'
        )
    else:
        return os.path.expanduser('~/.PyEveMap')


if __name__ == '__main__':
    exit(main())
