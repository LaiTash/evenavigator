import os
from datetime import timedelta, datetime
from time import sleep
from types import SimpleNamespace

import requests
from mock import mock
from pony.orm import Database

from app.environment import AppEnvironment
from app.ui.registertokenpopup import RegisterTokenPopup
import tempfile

from esi.ownership.pony import PonyTokenOwnersContainer
from esi.register import create_registrator_server
from esi.token.pony import PonyOwnedTokensContainer


class TestRegisterTokenPopup:
    def setup_method(self):
        self.tmpdir = tempfile.mkdtemp()
        self.database_filename = os.path.join(self.tmpdir, 'database.db')

        self.db = Database()
        self.db.bind('sqlite', self.database_filename, create_db=True)
        self.tokens = PonyOwnedTokensContainer(self.db)
        self.owners = PonyTokenOwnersContainer(self.db)
        self.db.generate_mapping(create_tables=True)

        self.security = SimpleNamespace(
            redirect_uri='http://localhost:800/auth',

            verify=lambda: {
                'CharacterID': 0,
                'CharacterName': "Lai Tash",
                'CharacterOwnerHash': '0'
            },
            auth=mock.MagicMock(),
            refresh_token='refresh',
            access_token='access',
            token_expiry=(datetime.utcnow() + timedelta(hours=1)).timestamp()
        )

        self.esi = SimpleNamespace(
            create_security=lambda: self.security,
            get_auth_uri=lambda: 'http://something.com/auth'
        )

        self.environment = AppEnvironment()
        self.environment.esi = self.esi
        self.environment.tokens = self.tokens
        self.environment.owners = self.owners

    @mock.patch('webbrowser.open', mock.Mock())
    def test_register(self):
        popup = RegisterTokenPopup(environment=self.environment)
        popup.dismiss = mock.Mock()
        popup.on_open()
        requests.get('{}?code=123'.format(self.security.redirect_uri))
        sleep(.1)
        refresh_token = next(iter(self.tokens.keys()))
        assert self.tokens[refresh_token].access_token == 'access'
        assert self.owners[refresh_token].character_name == 'Lai Tash'
        assert popup.dismiss.called


    @mock.patch('webbrowser.open', mock.Mock())
    def test_cancel(self):
        popup = RegisterTokenPopup(environment=self.environment)
        popup._start()
        popup.on_dismiss()
        assert popup._server_thread is None
