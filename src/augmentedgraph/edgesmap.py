from functools import partial
from typing import Mapping, Any, Tuple, MutableMapping, Iterator

import collections
from networkx import Graph, NetworkXError

from augmentedgraph.dictproxy import SimpleObservableDictProxy
from augmentedgraph.nodesmap import NodesMetaObserver, NodeId


class EdgesMetaObserver(object):
    def _on_set_edge_meta(
            self, u: NodeId, v: NodeId, key: Any, value: Any
    ) -> None:  # pragma: no cover
        pass

    def _on_delete_edge_meta(
            self, u: NodeId, v: NodeId, key: Any
    ) -> None:  # pragma: no cover
        pass


class NeighborsMap(Mapping[NodeId, dict]):
    def __init__(
            self, bunch: NodeId, graph: Graph, observer: EdgesMetaObserver
    ) -> None:
        self._bunch = bunch
        self._observer = observer
        self._graph = graph

    def __getitem__(self, item: NodeId) -> SimpleObservableDictProxy:
        subject = self._graph[self._bunch][item]
        on_set = partial(self._observer._on_set_edge_meta, self._bunch, item)
        on_delete = partial(self._observer._on_delete_edge_meta, self._bunch,
                            item)
        return SimpleObservableDictProxy(subject, on_set, on_delete)

    def __len__(self) -> int:
        return len(self._graph[self._bunch])

    def __iter__(self) -> Iterator[NodeId]:
        return iter(self._graph[self._bunch])

    def __contains__(self, item: NodeId) -> bool:
        return self._graph.has_edge(self._bunch, item)


class EdgesMap(Mapping[NodeId, NeighborsMap]):
    def __init__(self, graph: Graph, observer: EdgesMetaObserver) -> None:
        self._graph = graph
        self._observer = observer

    def __getitem__(self, item: NodeId) -> NeighborsMap:
        return NeighborsMap(item, self._graph, self._observer)

    def __len__(self) -> int:
        return self._graph.number_of_nodes()

    def __iter__(self) -> Iterator[NodeId]:
        return iter(self._graph.edge)

    def __contains__(self, item: NodeId) -> bool:
        return self._graph.has_edge(*item)

