"""

Provides a way for creating observable instances by subclassing
:py:class:`AugmentedGraph` it and overriding the callback methods.

For example, implementing a kivy EventDispatcher would look like this:

    .. code::
        >>> from kivy.event import EventDispatcher
        >>>
        >>>
        >>> class KivyGraph(AugmentedGraph, EventDispatcher):
        ...
        ...     def __init__(self, graph):
        ...         super().__init__(graph)
        ...         self.register_event_type('on_add_node')
        ...         self.register_event_type('on_set_node_meta')
        ...         # ... more events here
        ...
        ...     def on_add_node(self, node):
        ...         pass
        ...
        ...     def on_set_node_meta(self, node, key, value):
        ...         pass
        ...
        ...     def _on_add_node(self, node):
        ...         self.dispatch('on_add_node', node)
        ...
        ...     def _on_set_node_meta(self, node, key, value):
        ...         self.dispatch('on_set_node_meta', node, key, value)
        ...
        ...     # ... other methods
        ...
        >>> def on_set_node_meta_callback(instance, node, key, value):
        ...     print("Called!", node, key, value)
        ...
        >>> kivy_graph = KivyGraph(Graph())
        >>> kivy_graph.bind(on_set_node_meta=on_set_node_meta_callback)
        >>> # Create a node and fire 'on_add_node' event
        >>> kivy_graph.add_node(1)
        >>> # Add metadata to node and fire 'on_set_node_meta' event
        >>> kivy_graph.nodes[1]['some_meta'] = 'some value'
        Called! 1 some_meta some value


"""

from typing import Any

from networkx import Graph

from .nodesmap import NodesMapping, NodesMetaObserver, NodeId
from .edgesmap import EdgesMap, EdgesMetaObserver
from .dictproxy import SimpleObservableDictProxy

from networkx.readwrite import json_graph


class AugmentedGraph(NodesMetaObserver, EdgesMetaObserver):

    def __init__(self, graph: Graph) -> None:
        self._graph = graph

    def _on_meta_set(self, key: Any, value: Any) -> None:
        pass

    def _on_meta_del(self, key: Any) -> None:
        pass

    def _on_add_edge(self, u: NodeId, v: NodeId) -> None:
        pass

    def _on_delete_edge(self, u: NodeId, v: NodeId) -> None:
        pass

    def _on_add_node(self, node_id: NodeId) -> None:
        pass

    def _on_delete_node(self, node_id: NodeId) -> None:
        pass

    def add_node(self, node: NodeId, meta_dict:dict=None, **meta:Any) -> None:
        self._graph.add_node(node, meta_dict, **meta)
        self._on_add_node(node)

    def remove_node(self, node: NodeId) -> None:
        """ Remove node and all related edges """
        self.remove_node_edges(node)
        self._graph.remove_node(node)
        self._on_delete_node(node)

    def remove_node_edges(self, node: NodeId) -> None:
        """ Remove edges related to the node """
        """ Remove all edges related to node """
        for neighbor in self._graph.neighbors(node):
            self.remove_edge(node, neighbor)

    def add_edge(self,
                 u: NodeId,
                 v: NodeId,
                 meta_dict:dict=None,
                 **meta: Any
                 ) -> None:

        self._ensure_nodes_exist(u, v)
        meta.update(meta_dict or {})
        self._graph.add_edge(u, v, meta_dict, **meta)
        self._on_add_edge(u, v)

    def _ensure_nodes_exist(self, *nodes: NodeId) -> None:
        absent_nodes = set(nodes).difference(set(self.nodes))

        if absent_nodes:
            just_one_node = len(absent_nodes) == 1
            raise KeyError(
                'Node{} {} {} absent'.format(
                    '' if just_one_node else 's',
                    absent_nodes,
                    'is' if just_one_node else 'are'
                )
            )

    def remove_edge(self, u: NodeId, v: NodeId) -> None:
        self._graph.remove_edge(u, v)
        self._on_delete_edge(u, v)

    @property
    def meta(self) -> SimpleObservableDictProxy:
        """ Return observable graph metadata dictionary """
        return SimpleObservableDictProxy(
            self._graph.graph, self._on_meta_set, self._on_meta_del
        )

    @property
    def nodes(self) -> NodesMapping:
        return NodesMapping(self._graph, self)

    @property
    def edges(self) -> EdgesMap:
        return EdgesMap(self._graph, self)

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, AugmentedGraph):
            return False

        return (
            (self._graph.graph, self._graph.node, self._graph.edge) ==
            (other._graph.graph, other._graph.node, other._graph.edge)
        )
