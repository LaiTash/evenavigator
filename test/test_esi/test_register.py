import os
from datetime import datetime, timedelta
from threading import Thread
from types import SimpleNamespace
from urllib.request import urlopen
import time

import mock
import pytest
from . import *

from esi.register import decide_port_from_uri, create_registrator_server


class TestDecidePortFromUri:
    def test_decide_explicit(self):
        result = decide_port_from_uri('http://localhost:666/auth')
        assert result == 666

    def test_decide_http(self):
        result = decide_port_from_uri('http://localhost/auth')
        assert result == 80

    def test_decide_https(self):
        result = decide_port_from_uri('https://localhost/auth')
        assert result == 443

    def test_decide_unknown(self):
        with pytest.raises(KeyError):
            decide_port_from_uri('unknown://localhost/auth')


class TestRegistratorServer:
    def test_registrator(self, tmpdir):
        db_filename = str(tmpdir.join('temp.db'))

        db = Database()
        db.bind('sqlite', db_filename, create_db=True)
        tokens_container = PonyOwnedTokensContainer(db)
        owners_container = PonyTokenOwnersContainer(db)
        db.generate_mapping(create_tables=True)

        security = SimpleNamespace(
            redirect_uri='http://localhost:800/auth',

            verify=lambda: {
                'CharacterID': 0,
                'CharacterName': "Lai Tash",
                'CharacterOwnerHash': '0'
            },
            auth = mock.MagicMock(),
            refresh_token='refresh',
            access_token='access',
            token_expiry=(datetime.utcnow() + timedelta(hours=1)).timestamp()
        )

        shutdown_callback = mock.MagicMock()

        server = create_registrator_server(
            security, tokens_container, owners_container,
            shutdown_callback=shutdown_callback
        )

        thread = Thread(target=server.serve_forever)
        thread.start()

        urlopen('http://localhost:800/auth?code=123')
        time.sleep(.1)

        security.auth.assert_called_with('123')
        assert shutdown_callback.called
        assert 'refresh' in tokens_container
        assert owners_container['refresh'].character_name == 'Lai Tash'

        # exit nicely and clean up the mess
        thread.join()
        db.drop_all_tables(with_all_data=True)
        os.remove(db_filename)
