from concurrent.futures import Future
import io

from simpletools.exception import *
from simpletools.futures import unwrap_future


def test_unwrap_future_result_method():
    class ContainsCallback:
        @unwrap_future(raise_exception)
        def callback(self, future):
            assert future == 'ok' and self.__class__ is ContainsCallback

    future = Future()
    future.set_result('ok')

    ContainsCallback().callback(future)


def test_unwrap_future_result():
    @unwrap_future(raise_exception)
    def callback(future):
        assert future == 'ok'

    future = Future()
    future.set_result('ok')

    callback(future)


def test_unwrap_future_result_method_as_function():
    class ContainsCallback:
        def callback(self, future):
            assert future == 'ok' and self.__class__ is ContainsCallback

    future = Future()
    future.set_result('ok')

    unwrap_future(raise_exception, ContainsCallback().callback)(future)


def test_unwrap_future_result_as_function():
    def callback(future):
        assert future == 'ok'

    future = Future()
    future.set_result('ok')

    unwrap_future(raise_exception, callback)(future)


def test_unwrap_future_exception():
    stream = io.StringIO()

    @unwrap_future(LogExceptionHandler(stream.write, str))
    def callback(future):
        raise AssertionError("Called")

    future = Future()
    future.set_exception(Exception('raised'))

    callback(future)

    assert stream.getvalue() == 'raised'