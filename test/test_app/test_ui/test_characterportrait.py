from concurrent.futures import Future

import mock
from kivy.app import App

from app.environment import AppEnvironment
from app.ui.characterportrait import CharacterPortrait


class TestCharacterPortrait:
    def setup_method(self):
        self.environment = AppEnvironment()
        self.environment.portraits_cache = mock.Mock()

    def test_use_predownloaded_data(self):
        self.environment.portraits_cache = mock.Mock()
        self.environment.portraits_cache.get_downloaded_portraits = mock.Mock(
            return_value={
                64: 'px64x64.jpg', 128: 'px128x128.jpg', 256: 'px256x256.jpg'
            }
        )


        widget = CharacterPortrait(environment=self.environment)
        widget.width = 150
        widget.character_id = 0

        assert widget.source == 'px128x128.jpg'

    def test_request(self):
        self.environment.portraits_cache.get_downloaded_portraits = mock.Mock(
            side_effect=[None, {64: 'px64x64.jpg'}])
        result = Future()
        result.set_result(True)
        self.environment.request_portraits = mock.Mock(return_value=result)

        widget = CharacterPortrait(environment=self.environment)
        widget.width = 150
        widget.character_id = 0

        assert widget.source == 'px64x64.jpg'
