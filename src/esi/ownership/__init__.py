from collections import namedtuple
from typing import MutableMapping, Any, cast
from esi.token import TokenKey


class TokenOwner(object):
    """

    Token ownership data container.

    """

    def __init__(
            self, character_id: int, character_name: str, owner_hash: str
    ) -> None:
        self.character_id = character_id
        self.character_name = character_name
        self.owner_hash = owner_hash

    def as_tuple(self) -> tuple:
        return (self.character_id, self.character_name, self.owner_hash)

    def __repr__(self) -> str:
        result = \
            "<TokenOwner: character_id={}, character_name={}, owner_hash={}>"
        return result.format(*self.as_tuple())

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, self.__class__):
            return False

        other = cast(TokenOwner, other)
        return self.as_tuple() == other.as_tuple()

class TokenOwnersContainer(MutableMapping[TokenKey, TokenOwner]):
    """

    Dict-like token ownership data storage

    """
    def key_from_character_id(self, character_id: int):
        """ Return a key associated with character id

        :raise LookupError: No key is associated with the given character id
        """
        try:
            return next(key for key, item in self.items()
                        if item.character_id == character_id)
        except StopIteration:
            raise LookupError('No key associated with character id: {}'
                              .format(character_id))