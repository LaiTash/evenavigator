import os
from concurrent.futures import ThreadPoolExecutor, Future, as_completed
from functools import partial
from pathlib import Path

import requests
from kivy import Logger
import re
from typing import Dict, Optional

from pony.orm import Database

from app.database.portraits import Portraits
from esi.esifacade import AsyncEsiFacade
from esi.operations.characters import portrait



class CharacterPortraitCache(object):

    def __init__(
            self,
            db: Database,
            esi: AsyncEsiFacade,
            portraits_folder: str,
            concurrent_requests: int=10,
    ) -> None:
        self._portraits = Portraits(db)
        self._esi = esi
        self._portraits_folder = os.path.abspath(portraits_folder)

        self._executor = ThreadPoolExecutor(concurrent_requests)
        self._pending_portrait_requests: Dict[int, Future] = {}

        self._portraits_requested_for_charid = set()
        super().__init__()

    def get_downloaded_portraits(self, character_id: int) -> Dict[int, str]:
        """ Get downloaded portraits path

        :param character_id: character id
        :return: paths as Dict[size, path]
        """
        regexp = re.compile('(\d+)_(\d+)\..+')
        result = {}
        for filename in os.listdir(self._portraits_folder):
            match = regexp.match(filename)
            if not match:
                continue

            file_character_id, size = match.groups()
            file_character_id, size = int(file_character_id), int(size)
            if file_character_id == character_id:
                result[size] = os.path.join(self._portraits_folder, filename)

        return result

    def request_portraits(self, character_id: int) -> Future:
        future = self._pending_portrait_requests.get(character_id)
        return future or self._submit_request_for_portraits(character_id)

    def _submit_request_for_portraits(self, character_id: int) -> Future:
        assert character_id not in self._pending_portrait_requests
        future = self._executor.submit(self._request_portraits_task,
                                       character_id)
        self._pending_portrait_requests[character_id] = future
        future.add_done_callback(
            partial(self._remove_pending_portrait_request, character_id)
        )
        return future

    def _remove_pending_portrait_request(self,
                                         character_id: int,
                                         _: Optional[Future]=None) -> None:
        del self._pending_portrait_requests[character_id]

    def _request_portraits_task(self, character_id: int) -> Dict[int, str]:
        # async_request is used to comply with global concurrent requests limit
        future = self._esi.async_request(portrait(character_id))
        exception = future.exception()
        if exception:
            Logger.error('Portraits requests failed: {}'.format(exception))
            return {}

        self._portraits.update_from_esi(character_id, future.result())
        self._download_character_portraits(character_id)

    def _download_character_portraits(self, character_id: int):
        records = self._portraits.character_records(character_id)
        executor = ThreadPoolExecutor(len(records))
        futures_list = [
            executor.submit(self._download_portrait, character_id, record.key)
            for record in records
        ]
        for future in as_completed(futures_list):
            exception = future.exception()
            if exception:
                Logger.error('Download failed: {}'.format(exception))

    def _download_portrait(self, character_id: int, key: str) -> None:
        record = self._portraits[character_id, key]
        response = requests.get(record.url)
        if response.status_code != 200:
            raise RuntimeError('Download failed')

        size = self._size_from_key(record.key)
        extension = os.path.splitext(record.url)[1]
        path = self._get_local_portrait_path(character_id, size, extension)
        print(path)
        with open(path, 'wb') as f:
            f.write(response.content)

    def _get_local_portrait_path(self,
                                 character_id: int,
                                 size: int,
                                 extension: str) -> str:
        filename = '{}_{}{}'.format(character_id, size, extension)
        return str(Path(self._portraits_folder).joinpath(filename).absolute())

    @staticmethod
    def _size_from_key(key: str) -> int:
        result = {
            'px64x64': 64,
            'px128x128': 128,
            'px256x256': 256,
            'px512x512': 512,
        }.get(key)
        if result is None:
            raise ValueError("Cannot derive size from: {}".format(key))
        return result
