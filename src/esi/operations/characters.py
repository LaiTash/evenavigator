from ..operationdef import OperationDef


def location(character_id: int) -> OperationDef:
    return OperationDef(
        'get_characters_character_id_location', character_id=character_id
    )


def portrait(character_id: int) -> OperationDef:
    return OperationDef(
        'get_characters_character_id_portrait', character_id=character_id
    )
