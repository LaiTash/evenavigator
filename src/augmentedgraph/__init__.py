"""

`augmentedgraph` package provides a way of creating observable adapters for
networkx Graph objects.

It doesn't implement an observer pattern on it's own: instead, it allows
creation an observable object by subclassing :py:class:`AugmentedGraph` and
overriding it's callback methods.

 """

from .augmentedgraph import AugmentedGraph