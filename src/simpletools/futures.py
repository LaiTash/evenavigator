from functools import wraps
from typing import Callable, Any, Optional, Union

from simpletools.exception import ExceptionHandler


__all__ = ['unwrap_future']


FutureCallback = Callable[..., Any]


class UnwrapFuture(object):
    def __init__(self, exception_handler: ExceptionHandler) -> None:
        self._exception_handler = exception_handler

    def __call__(self, method: FutureCallback) -> Callable:

        @wraps(method)
        def _wrapper(instance: Any, future=None) -> Callable:  # type: ignore
            future_arg = future or instance

            exception = future_arg.exception()
            if exception is not None:
                return self._exception_handler(exception)

            result = future_arg.result()

            if future:
                return method(instance, result)
            else:
                return method(result)

        return _wrapper



def unwrap_future_function(
        exception_handler: ExceptionHandler, callback: FutureCallback
) -> Callable:
    return UnwrapFuture(exception_handler)(callback)


def unwrap_future(
        exception_handler: ExceptionHandler,
        callback: Optional[FutureCallback]=None
) -> Union[UnwrapFuture, Callable]:
    """ Decorates a callable, unwrapping a Future object passed to it

    .. code::

        >>> from concurrent.futures import ThreadPoolExecutor
        >>> import time
        >>>
        >>> def good_execution_target():
        ...     time.sleep(.01)
        ...     return "It works!"
        ...
        >>> def bad_execution_target():
        ...     time.sleep(.01)
        ...     raise Exception('It never meant to work')
        ...
        >>> def callback(result):
        ...     print(result)
        ...
        >>> unwrapped_callback = unwrap_future(print, callback)
        >>> executor = ThreadPoolExecutor(1)
        >>> future = executor.submit(good_execution_target)
        >>> future.add_done_callback(unwrapped_callback)
        >>> time.sleep(.02)
        It works!
        >>> future = executor.submit(bad_execution_target)
        >>> future.add_done_callback(unwrapped_callback)
        >>> time.sleep(.02)
        It never meant to work

    :param exception_handler: an exception handler what will be called if
        an exception was raised during the execution.
    :param callback: a callable to decorate. If omitted, `unwrap_future` acts
        as a decorator.
    :return:
    """
    if callback:
        return unwrap_future_function(exception_handler, callback)

    return UnwrapFuture(exception_handler)

