from typing import Tuple, Optional, cast

import numpy as np


Coord = Tuple[float, float]


def _scale_matrix(factor: float, origin: np.ndarray) -> np.array:
    result = np.diag([factor, factor, 1.0])

    if origin is not None:
        result[:2, 2] = origin[:2]
        result[:2, 2] *= 1.0 - factor

    return result


def _translation_matrix(direction: np.ndarray) -> np.array:
    result = np.identity(3)
    result[:2, 2] = direction[:2]

    return result


class Viewport(object):
    _aspect_ratio: float
    _transform_matrix: np.array
    _stretch_matrix: np.array
    _matrix: np.array

    def __init__(self, aspect_ratio:float=1) -> None:
        self._transform_matrix = np.identity(3)
        self._matrix = np.identity(3)
        self.aspect_ratio = aspect_ratio

    def _update_matrix(self) -> None:
        self._matrix = self._stretch_matrix.dot(self._transform_matrix)

    @property
    def aspect_ratio(self) -> float:
        return self._aspect_ratio

    @aspect_ratio.setter
    def aspect_ratio(self, value: float) -> None:
        self._aspect_ratio = value
        self._stretch_matrix = np.array([[1, 0, 0], [0, value, 0], [0, 0, 1]])
        self._update_matrix()

    def set_aspect_ratio(self, value: float) -> None:
        self.aspect_ratio = value

    @property
    def height(self) -> float:
        return 1 / self.aspect_ratio

    @property
    def diagonal(self) -> float:
        return (1 + self.height ** 2) ** 0.5

    def translate(self, x: float, y: float) -> None:
        matrix = _translation_matrix(np.array([x, y]))
        self._transform_matrix = matrix.dot(self._transform_matrix)
        self._update_matrix()

    def scale(self, factor: float, origin: Optional[Coord]=None) -> None:
        if origin is not None:
            origin = np.array(origin)
        matrix = _scale_matrix(factor, origin)
        self._transform_matrix = matrix.dot(self._transform_matrix)
        self._update_matrix()

    def layout_to_viewport(self, point: Coord) -> Coord:
        point = np.append(point, [1])
        point = self._matrix.dot(point)[:2]

        point = cast(Coord, tuple(point))   # ignore complaints
        return point

    def viewport_to_layout(self, point: Coord) -> Coord:
        inverted_matrix = np.linalg.inv(self._matrix)

        point = np.append(point, [1])
        point = inverted_matrix.dot(point)[:2]

        point = cast(Coord, tuple(point))  # ignore complaints
        return point
