import pytest
import mock
from augmentedgraph.nodesmap import NodesMapping, NodesMetaObserver


@pytest.fixture
def nodes_map(networkx_graph):
    class _Observer(NodesMetaObserver):
        _on_set_node_meta = mock.MagicMock()
        _on_delete_node_meta = mock.MagicMock()
    return NodesMapping(networkx_graph, _Observer())

def test_getitem(nodes_map):
    # Must return existing node's meta
    assert nodes_map[1]._subject is nodes_map._graph.node[1]

def test_meta_changes_notify_observer(nodes_map):
    nodes_map._graph.add_node(1, meta='value')
    nodes_map[1]['meta'] = 'another value'
    nodes_map._observer._on_set_node_meta.assert_called_with(
        1, 'meta', 'another value'
    )

def test_meta_deletes_notify_observer(nodes_map):
    nodes_map._graph.add_node(1, meta='value')
    del nodes_map[1]['meta']
    nodes_map._observer._on_delete_node_meta.assert_called_with(1, 'meta')

def test_iter(nodes_map):
    # Must return graph nodes id's
    assert set(nodes_map) == set(nodes_map._graph.nodes())

def test_len(nodes_map):
    # Must return the number of nodes in the graph
    assert len(nodes_map) == nodes_map._graph.number_of_nodes()
