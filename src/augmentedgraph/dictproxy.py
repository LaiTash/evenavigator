from typing import MutableMapping, Callable, Any, Iterator


class SimpleObservableDictProxy(MutableMapping):
    """

    A simple observable dict proxy

    Usage:

        .. code::

            >>> def on_set(k, v): print("Set!", k, v)
            >>> def on_delete(k): print("Delete!", k)
            >>>
            >>> proxy = SimpleObservableDictProxy(
            ...     {'mykey': 'myvalue'},
            ...     on_set=on_set, on_delete=on_delete
            ... )
            ...
            >>> proxy['mykey'] = 'another value'
            Set! mykey another value
            >>> del proxy['mykey']
            Delete! mykey

    """

    def __init__(
            self,
            subject: dict,
            on_set: Callable[[Any, Any], None],
            on_delete: Callable[[Any], None]
    ) -> None:
        self._on_set = on_set
        self._on_del = on_delete
        self._subject = subject

    def __getitem__(self, item: Any) -> Any:
        return self._subject.__getitem__(item)

    def __iter__(self) -> Iterator[Any]:
        return self._subject.__iter__()

    def __len__(self) -> int:
        return self._subject.__len__()

    def __setitem__(self, key: Any, value: Any) -> None:
        self._subject.__setitem__(key, value)
        self._on_set(key, value)

    def __delitem__(self, key: Any) -> None:
        self._subject.__delitem__(key)
        self._on_del(key)