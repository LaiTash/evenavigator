import mock
import networkx
import pytest

from augmentedgraph.edgesmap import NeighborsMap
from . import EdgeTester


class TestEdges(EdgeTester):
    def test_getitem(self):
        source, dest = self.create_edge(meta='value')

        neighbors = self.edges_map[source]
        assert isinstance(neighbors, NeighborsMap)
        assert neighbors._bunch == source

    def test_len(self):
        a, b, c, d = self.create_multiple_nodes(4)
        self.graph.add_edge(a, b)
        self.graph.add_edge(a, c)
        self.graph.add_edge(c, a)
        self.graph.add_edge(b, c)

        assert len(self.edges_map) == 4

    def test_iter(self):
        a, b, c, d = self.create_multiple_nodes(4)
        self.graph.add_edge(a, b)
        self.graph.add_edge(a, c)
        self.graph.add_edge(c, a)
        self.graph.add_edge(b, c)

        assert set(iter(self.edges_map)) == {a, b, c, d}

    def test_contains(self):
        a, b, c, d = self.create_multiple_nodes(4)
        self.graph.add_edge(a, b)

        assert (a, b) in self.edges_map
        assert (b, c) not in self.edges_map
