"""

Exception handler is an callable object that receives an exception object
and just... does something with it. It is useful for decorators, wrappers,
proxies, and all that stuff.

"""


from traceback import *
from typing import Callable


__all__ = [
    'ExceptionHandler', 'ExceptionFormatter',
    'raise_exception',
    'format_exception_traceback',
    'LogExceptionHandler'
]


ExceptionHandler = Callable[[Exception], None]
ExceptionFormatter = Callable[[Exception], str]


def raise_exception(exception: Exception) -> None:
    """ A simple ExceptionHandler that just re-raises the exception """
    raise exception


def format_exception_traceback(exception: Exception) -> str:
    """ Return the exception traceback as a string

    Designed to be used with :py:class:`LogExceptionHandler`
    """
    return ''.join(
        format_exception(
            exception.__class__, exception, exception.__traceback__
        )
    )


class LogExceptionHandler:
    """

    Exception handler that is initialized with two callables: one is used
    to convert an exception to string in some way, and the other is the
    actual handler that receives that string as an argument. Useful for
    logging.

    .. code::

        >>> handler=LogExceptionHandler(print, format_exception_traceback)
        >>> handler(Exception("something bad had just happened"))
        Exception: something bad had just happened
        ...

    """
    def __init__(
            self,
            handler: Callable[[str], None],
            formatter:ExceptionFormatter
    ) -> None:
        self._handler = handler
        self._formatter = formatter

    def __call__(self, exception: Exception) -> None:
        message = self._formatter(exception)
        self._handler(message)