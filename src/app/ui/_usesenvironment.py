from typing import Optional, Any

from kivy.app import App

from app.environment._environment import AppEnvironment


class UsesEnvironment:
    def __init__(self, environment: Optional[AppEnvironment]=None) -> None:
        self._environment = environment

    @property
    def environment(self) -> AppEnvironment:
        return self._environment or App.get_running_app().environment

