from pony.orm import Database
from itertools import chain

from app.database.constellations import Constellations
from app.database.regions import Regions
from app.database.stargates import Stargates
from app.database.starsystems import StarSystems
from esi.esifacade import AsyncEsiFacade
from esi.operations.universe import regions, systems, constellations, stargates


__all__ = ['rebuild_eve_data']


def rebuild_eve_data(esi: AsyncEsiFacade, db: Database) -> None:
    """ Request and rebuild eve data

        * Star systems
        * Constellations
        * Regions
        * Stargates

    .. note::
        It is a blocking procedure

    .. waring::

        The existing data will be deleted

    :param exception_handler:
    :param esi:
    :param db:
    :return:
    """

    region_ids = esi.request(regions())
    regions_data = esi.carefully_request_many(map(regions, region_ids))
    Regions(db).recreate_from_esi(regions_data)

    constellations_ids = esi.request(constellations())
    constellations_data = esi.request_many(
        map(constellations, constellations_ids)
    )
    Constellations(db).recreate_from_esi(constellations_data)

    systems_ids = esi.request(systems())

    assert len(set(systems_ids)) == len(systems_ids)

    systems_data = list(esi.carefully_request_many(map(systems, systems_ids)))
    StarSystems(db).recreate_from_esi(systems_data)

    stargate_ids = chain(
        *map(lambda s: s.get('stargates', None), systems_data)
    )



    stargates_data = esi.carefully_request_many(map(stargates, stargate_ids))
    Stargates(db).recreate_from_esi(stargates_data)

