"""

Allows validation and serialization/deserialization of json objects with
kivy EventDispatcher objects.

.. code::

    >>> from kivy.properties import *
    >>> class Cat(KeyAccessConfigContainer):
    ...     name = StringProperty()
    ...     # cat's don't live for long :(
    ...     age = BoundedNumericProperty(0, min=0, max=30)
    ...
    >>> class CatOwner(KeyAccessConfigContainer):
    ...     name = StringProperty()
    ...     cat = Cat
    ...
    >>> def callback(instance, value):
    ...     print("The cat's new name is %s" % value)
    ...
    >>> owner = CatOwner.from_json(
    ...     {'name': 'Bob', 'cat': {'name': 'felix', 'age':5} }
    ... )
    ...
    >>> owner.cat.bind(name=callback)
    >>> owner.cat.name = 'Spot'
    The cat's new name is Spot

"""
from .config import KeyAccessConfigContainer