import pytest

from app.environment.folders import EnvironmentFolders


@pytest.fixture
def base_folder(tmpdir):
    return str(tmpdir.join('.evenavigator'))


@pytest.fixture
def folders_object(base_folder):
    return EnvironmentFolders(base_folder)
