import os
import tempfile
from pathlib import Path
from time import sleep
from types import SimpleNamespace
from urllib.parse import urlparse
from urllib.request import url2pathname

import mock
from pony.orm import Database

from app.database import bind_evenavigator_tables
from app.environment.portraitmanager import CharacterPortraitCache


def local_get(url):
    """ Fetch a stream for local files """
    parsed_url = urlparse(url)
    if parsed_url.scheme != 'file':
        raise ValueError("Local path expected")

    path = url2pathname(parsed_url.path)
    if not os.path.exists(path):
        return SimpleNamespace(status_code=404)

    return SimpleNamespace(
        content=open(path, 'rb').read(),
        status_code=200
    )



class TestPortraitManager:

    def setup_method(self, method):
        self.tmpdir = tempfile.mkdtemp()
        self.portraits_folder = os.path.join(self.tmpdir, 'portraits_folder')
        os.makedirs(self.portraits_folder, exist_ok=True)
        self.setup_database()
        self.setup_esi()
        self.setup_portrait_manager()

    def setup_database(self):
        self.db_filename = os.path.join(self.tmpdir, 'database.db')
        self.db = Database()
        self.db.bind('sqlite', self.db_filename, create_db=True)
        bind_evenavigator_tables(self.db)
        self.db.generate_mapping(create_tables=True)

    def setup_esi(self):
        self.esi = mock.Mock()
        self.esi.async_request = mock.Mock()

    def setup_portrait_manager(self):
        self.portrait_manager = CharacterPortraitCache(
            self.db, self.esi, self.portraits_folder
        )

    def set_esi_result(self, data):
        result = mock.Mock()
        self.esi.async_request.return_value = result
        result.exception = mock.Mock(return_value=None)
        result.result = mock.Mock(return_value=data)

    def create_dummy_file(self, filename, contents='dummy'):
        with open(filename, 'w') as f:
            f.write(contents)

    def create_dummy_file_url(self, filename):
        path = Path(os.path.join(self.tmpdir, filename))
        self.create_dummy_file(str(path), filename)
        return path.as_uri()

    def create_dummy_file_in_portraits_folder(self, filename):
        path = os.path.join(self.portraits_folder, filename)
        self.create_dummy_file(path, filename)
        return path

    def test_get_downloaded_portraits(self):
        expected = {
            64:  self.create_dummy_file_in_portraits_folder('12345_64.jpg'),
            128: self.create_dummy_file_in_portraits_folder('12345_128.png'),
            92: self.create_dummy_file_in_portraits_folder('12345_92.jpg')
        }

        # Not to be found in results
        self.create_dummy_file_in_portraits_folder('67890_64.jpg'),
        self.create_dummy_file_in_portraits_folder('abracadabra.png')

        result = self.portrait_manager.get_downloaded_portraits(12345)
        assert result == expected

    @mock.patch('requests.get', local_get)
    def test_request_portraits_new(self):
        downloaded_results = {
            64: 'px64x64.jpg',
            128: 'px128x128.jpg'
        }
        esi_response_data = {
            'px{s}x{s}'.format(s=size): self.create_dummy_file_url(filename)
            for size, filename in downloaded_results.items()
        }
        self.set_esi_result(esi_response_data)
        future = self.portrait_manager.request_portraits(0)
        future.result()  # wait for completion

        results = self.portrait_manager.get_downloaded_portraits(0)
        for size, filename in results.items():
            print(filename)
            contents = open(filename, 'r').read()
            assert contents == downloaded_results[size]

    def test_request_portraits_existing(self):
        def _mocked_task(character_id):
            sleep(.1)

        self.portrait_manager._request_portraits_task = _mocked_task

        future_1 = self.portrait_manager.request_portraits(0)
        future_2 = self.portrait_manager.request_portraits(0)

        assert future_1 is future_2