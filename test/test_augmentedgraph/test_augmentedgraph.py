import random
from typing import Type

import mock
import pytest

import itertools

from augmentedgraph.augmentedgraph import AugmentedGraph

from faker import Factory as FakerFactory
import networkx


fake = FakerFactory.create()


def create_random_graph(max_nodes=10):
    graph = networkx.Graph()
    node_keys = list(fake.pyset(nb_elements=max_nodes))
    for key in node_keys:
        graph.add_node(key, {k:v for k, v in fake.pydict().items() if k!='id'})

    edges = filter(
        lambda _: random.random() > .5,
        itertools.permutations(node_keys, 2)
    )

    for u, v in edges:
        graph.add_edge(u, v, fake.pydict())

    graph.graph.update(fake.pydict())

    return graph


def create_equal_graphs(max_nodes=10):
    state = random.getstate()
    fake_state = fake.random.getstate()
    first_graph = create_random_graph(max_nodes)

    random.setstate(state)
    fake.random.setstate(fake_state)
    second_graph = create_random_graph(max_nodes)

    return first_graph, second_graph


class AugmentedGraphTester:

    def setup_method(self):
        self.graph = networkx.Graph()
        self.augmented_graph = AugmentedGraph(self.graph)

    def create_nodes(self, *nodes):
        for node in nodes:
            self.graph.add_node(node)

    def create_edge(self, u, v):
        self.create_nodes(u, v)
        self.graph.add_edge(u, v)


class TestProperties(AugmentedGraphTester):
    def test_meta_property_is_correct(self):
        meta = self.augmented_graph.meta
        assert meta._subject is self.graph.graph
        assert meta._on_set == self.augmented_graph._on_meta_set
        assert meta._on_del == self.augmented_graph._on_meta_del

    def test_nodes_property_is_correct(self):
        nodes = self.augmented_graph.nodes
        assert nodes._graph is self.graph
        assert nodes._observer is self.augmented_graph

    def test_edges_property_is_correct(self):
        edges = self.augmented_graph.edges
        assert edges._graph is self.graph
        assert edges._observer is self.augmented_graph


class TestOther(AugmentedGraphTester):
    @pytest.mark.parametrize(
        'first,second', [
            create_equal_graphs() for i in range(10)
        ]
    )
    def test_eq(self, first, second):
        first, second = map(AugmentedGraph, (first, second))
        assert first == second

    @pytest.mark.parametrize(
        'first,second', [
            (create_random_graph(), create_random_graph())
            for i in range(10)
        ]
    )
    def test_neq(self, first, second):
        first, second = map(AugmentedGraph, (first, second))
        assert first != second


class TestNodes:

    class TestAddNode(AugmentedGraphTester):
        def test_add_node(self):
            # Must add node with correct meta
            self.augmented_graph.add_node(1, {'a': 'b'}, c='d', e='f')
            assert self.graph.node[1] == {'a': 'b', 'c': 'd', 'e': 'f'}

        def test_add_node_notify_self(self):
            # Must call on_add_node when node is added
            self.augmented_graph._on_add_node = mock.MagicMock()
            self.augmented_graph.add_node(1, {'a': 'b'}, c='d', e='f')
            self.augmented_graph._on_add_node.assert_called_with(1)

    class TestRemoveNode(AugmentedGraphTester):
        def test_remove_node(self):
            # Must remove node
            self.augmented_graph.add_node(1)
            self.augmented_graph.remove_node(1)

            assert not self.graph.has_node(1)

        def test_remove_node_notify_self(self):
            # Must call on_delete_node when node is removed
            self.augmented_graph._on_delete_node = mock.MagicMock()

            self.augmented_graph.add_node(1)
            self.augmented_graph.remove_node(1)

            self.augmented_graph._on_delete_node.assert_called_with(1)

        def test_remove_node_removes_edges(self):
            # Must remove all related edges, but not unrelated ones
            self.augmented_graph._on_delete_edge = mock.MagicMock()

            self.create_nodes(1, 2, 3)
            self.create_edge(4, 5)
            self.graph.add_edge(1, 2)
            self.graph.add_edge(1, 3)
            self.graph.add_edge(1, 4)

            self.augmented_graph.remove_node(1)

            for dest in (2, 3, 4):
                assert not self.graph.has_edge(1, dest)
                self.augmented_graph._on_delete_edge.assert_any_call(1, dest)

            assert self.graph.has_edge(4, 5)


class TestEdges:
    """ Methods related to edges """

    class TestAddEdge(AugmentedGraphTester):
        """ py:method:`AugmentedGraph.add_edge` """

        def test_add_edge(self):
            # Must add edges with correct meta
            u, v = 1, 2
            self.create_nodes(u, v)
            self.augmented_graph.add_edge(u, v, {'a': 'b'}, c='d', e='f')

            assert self.graph.edge[u][v] == {'a': 'b', 'c': 'd', 'e': 'f'}
            # Reverse should be correct as well
            assert self.graph.edge[v][u] == {'a': 'b', 'c': 'd', 'e': 'f'}

        def test_source_node_absent(self):
            # Should raise KeyError if source node is absent
            self.augmented_graph.add_node(2)
            with pytest.raises(KeyError):
                self.augmented_graph.add_edge(1, 2)

        def test_dest_node_absent(self):
            # Should raise KeyError if destination node is absent
            self.augmented_graph.add_node(1)
            with pytest.raises(KeyError):
                self.augmented_graph.add_edge(1, 2)

        def test_notify_self(self):
            # Must call on_add_edge when edge is added
            u, v = 1, 2
            self.augmented_graph._on_add_edge = mock.MagicMock()

            self.create_nodes(u, v)
            self.augmented_graph.add_edge(u, v)

            self.augmented_graph._on_add_edge.assert_called_with(u, v)

    class TestRemoveEdge(AugmentedGraphTester):
        """ py:method:`AugmentedGraph.remove_edge` """

        def test_remove_edge_straight_order(self):
            # Must remove edge
            u, v = 1, 2
            self.create_edge(u, v)

            self.augmented_graph.remove_edge(u, v)

            assert not self.graph.has_edge(u, v)

        def test_remove_edge_reverse_order(self):
            # Must remove edge
            u, v = 1, 2
            self.create_edge(u, v)

            self.augmented_graph.remove_edge(v, u)

            assert not self.graph.has_edge(u, v)

        def test_notify_self(self):
            # Must call on_delete_edge when edge is removed
            self.augmented_graph._on_delete_edge = mock.MagicMock()

            u, v = 1, 2
            self.create_edge(u, v)

            self.augmented_graph.remove_edge(u, v)

            self.augmented_graph._on_delete_edge.assert_called_with(u, v)
