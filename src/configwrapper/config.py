from abc import ABCMeta, abstractmethod, abstractclassmethod
from typing import Iterable, Tuple, Type, Any, Container, Dict

from kivy.event import EventDispatcher
from kivy.properties import Property, ReferenceListProperty


class DispatchingConfigContrainer(EventDispatcher, metaclass=ABCMeta):
    def __init__(self) -> None:
        super().__init__()
        self.register_event_type('on_change')

    def on_change(self) -> None:
        pass

    def dispatch_on_change(self, *args: Any) -> None:
        self.dispatch('on_change')

    @abstractmethod
    def to_json(self) -> Container:
        pass

    @abstractclassmethod
    def from_defaults(cls) -> 'DispatchingConfigContrainer':
        pass

    @abstractmethod
    def from_json(
            cls, obj: Any, allow_unknown: bool=False
    ) -> 'DispatchingConfigContrainer':
        pass

class KeyAccessConfigContainer(DispatchingConfigContrainer):

    def __init__(self) -> None:
        super().__init__()
        for member_name, member in self.get_properties():
            self.bind(**{member_name: self.dispatch_on_change})

    def _hook_changes(self) -> None:
        for member_name, _ in self._get_branches():
            getattr(self, member_name).bind(on_change=self.dispatch_on_change)

    @classmethod
    def from_defaults(cls) -> 'KeyAccessConfigContainer':
        result = cls()
        for child_name, child_cls in cls._get_branches():
            defaults = child_cls.from_defaults()  # type: ignore
            setattr(result, child_name, defaults)
        result._hook_changes()
        return result

    @classmethod
    def from_json(
            cls, obj: Dict, allow_unknown:bool=False
    ) -> 'KeyAccessConfigContainer':
        result = cls()
        branches = dict(cls._get_branches())

        for key, value in obj.items():
            if not (allow_unknown or hasattr(result, key)):
                raise ValueError(key)

            branch = branches.get(key)
            if branch is not None:
                child = branch.from_json(value, allow_unknown)
                del branches[key]
            else:
                child = value

            setattr(result, key, child)

        for key, member_cls in branches.items():
            defaults = member_cls.from_defaults()  # type: ignore
            setattr(result, key, defaults)

        result._hook_changes()
        return result

    @classmethod
    def get_properties(cls) -> Iterable[Tuple[str, Property]]:
        for member_name in dir(cls):
            member = getattr(cls, member_name)
            is_property = isinstance(member, Property)
            if_reflist = isinstance(member_name, ReferenceListProperty)
            if is_property and not if_reflist:
                yield member_name, member

    @classmethod
    def _get_branches(cls)\
            -> Iterable[Tuple[str, Type[DispatchingConfigContrainer]]]:
        for member_name in dir(cls):
            member = getattr(cls, member_name)

            if (
                isinstance(member, type)
                and issubclass(member, DispatchingConfigContrainer)
            ):
                yield member_name, member

    def to_json(self) -> dict:
        result = {}
        for name, value in self.get_properties():
            result[name] = getattr(self, name)

        for name, branch in self._get_branches():
            result[name] = getattr(self, name).to_json()

        return result