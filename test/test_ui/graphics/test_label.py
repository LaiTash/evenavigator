import os
os.environ['KIVY_IMAGE'] = 'pil'

from warnings import warn

from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle

from ui.graphics.label import Label
from ui.graphics.testing import *


class TestLabel:

    @pytest.mark.parametrize(
        'pos,size,text_area_scale,label_kw', [
            [(0, 0), (600, 600), 1, {'font_size': 100}],
            [(0, 0), (600, 600), 0.8, {'font_size': 100}],
            [(0, 0), (120, 60), 0.8, {'font_size': 100}],
            [(0, 0), (120, 600), 0.8, {'font_size': 100}],
            [(0, 0), (60, 120), 0.8, {'font_size': 100}],
        ]
    )
    def test_label(self, tmpdir, pos, size, text_area_scale, label_kw):
        Widget()
        filename = '{}_{}_{}.png'.format(pos, size, text_area_scale)
        base_folder = os.path.abspath(os.path.dirname(__file__))
        examples_folder = os.path.join(base_folder, 'examples')
        os.makedirs(examples_folder, exist_ok=True)

        example_filename = os.path.join(examples_folder, filename)

        with render_to_texture() as texture:
            Color(rgba=(1, 0, 0, 1))
            Rectangle(pos=pos, size=size)
            Label(
                "Uke Nyasha",
                pos=pos, size=size, text_area_scale=text_area_scale,
                **label_kw
            )

        if not os.path.exists(example_filename):
            texture.save(example_filename)
            raise Exception("Example image `{}` was not found and needed to "
                            "be created. Check if it's correct and re-run the "
                            "test")

            return

        tested_filename = str(tmpdir.join(filename))
        texture.save(tested_filename)

        comparison_result = compare_image_files(tested_filename,
                                                example_filename)

        assert comparison_result == 0

