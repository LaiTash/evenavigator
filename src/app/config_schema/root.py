from kivy.properties import StringProperty, BoundedNumericProperty

from configwrapper.config import KeyAccessConfigContainer
from .esi import ESISchema


class RootSchema(KeyAccessConfigContainer):
    database_filename = StringProperty('database.db')
    max_threads = BoundedNumericProperty(100, min=1)
    character_portraits_cache_folder = StringProperty('portraits_cache')
    esi = ESISchema

