from collections import namedtuple
from contextlib import contextmanager
from functools import partial
from time import sleep
from typing import Optional, Iterable, Type, ClassVar, Any, Iterator

from esipy import App, EsiClient, EsiSecurity
from pyswagger.io import Response
from pyswagger.primitives import Model

from .operationdef import OperationDef, OperationFactory, bind_operation_def, \
    Operation
from .token import Token
from concurrent.futures import Executor, Future

SecuritySpecs = namedtuple(
    'SecuritySpecs',
    ['client_id', 'secret_key', 'redirect_uri', 'scopes']
)


class EsiError(Exception):
    def __init__(self, error):
        super().__init__(error.data.get('error', 'No message'))


class EsiFacade(object):
    """

    Facade objects for creating EsiSecurity and EsiClient objects and making
    requests on behalf of that objects.

    .. code::

        >>> specs = SecuritySpecs(
        ...     client_id='16e7faao5493123eb36b33e047f2b731',
        ...     secret_key='rx3Mw9PQkuEHy4j0hBMeASNHl6txg3gI4peOgq8r',
        ...     redirect_uri='http://localhost/auth',
        ...     scopes=[]
        ... )
        ...
        >>> app = App(
        ...    'https://esi.tech.ccp.is/v1/swagger.json?datasource=tranquility'
        ... )
        >>> facade = EsiFacade(app, specs)

    """

    app: App
    __security_specs: SecuritySpecs

    def __init__(self,
                 app: App,
                 security_specs: SecuritySpecs,
                 ) -> None:
        """

        :param app: Esi application object
        :param security_specs: security specifications to use for creating
            security objects
        """

        self.app = app
        self.__security_specs = security_specs

    @staticmethod
    def __make_request(client: EsiClient, operation: Operation) -> Response:
        return client.request(operation)

    def get_auth_uri(self):
        return self.create_security().get_auth_uri(
            self.__security_specs.scopes
        )

    def create_security(self, token: Optional[Token]=None) -> EsiSecurity:
        """ Create a security object """
        result = EsiSecurity(
            self.app,
            redirect_uri=self.__security_specs.redirect_uri,
            client_id=self.__security_specs.client_id,
            secret_key=self.__security_specs.secret_key
        )

        if token is not None:
            result.update_token(token.to_standard_json())

        return result

    def create_operation(self, operation_def: OperationDef) -> Operation:
        """ Bound an operation definition to application """
        return bind_operation_def(self.app, operation_def)

    def create_client(self, token: Optional[Token]=None) -> EsiClient:
        """ Create a client object """
        return EsiClient(self.create_security(token))

    def _request(self,
                 operation_def: OperationDef,
                 token: Optional[Token]=None) -> Model:
        client = self.create_client(token)
        operation = self.create_operation(operation_def)
        result = self.__make_request(client, operation)

        result_ok = 0 <= result.status - 200 < 100
        if not result_ok:
            raise EsiError(result)

        return result.data

    def request(
            self, operation_def: OperationDef, token: Optional[Token]=None
    ) -> Model:
        """ Make an ESI request

        :param operation_def: operation definition
        :param token: authentication token, if neede
        :return: response object
        """
        return self._request(operation_def, token)


class AsyncEsiFacade(EsiFacade):
    """ EsiFacade subclass capable of making asynchronous requests """

    _pool: Executor

    def __init__(
            self, pool: Executor, app: App, security_specs: SecuritySpecs
    ) -> None:
        """

        :param pool: Executor object to use
        :param app: Esi application object
        :param security_specs: security specifications to use for creating
            security objects
        """
        super().__init__(app, security_specs)
        self._pool = pool

    def async_request(
            self, operation: OperationDef, token: Optional[Token]=None,
    ) -> Future:
        """ Create an asynchronous request and return a Future object

        :param operation: operation definition
        :param token: security token, if needed
        :return: Future object
        """

        return self._pool.submit(self._request, operation, token)

    def request_many(
            self,
            operations: Iterable[OperationDef], token: Optional[Token]=None,
            timeout: Optional[float]=None,
    ):
        return self._pool.map(
            partial(self._request, token=token), operations,
            timeout=timeout
        )

    def carefully_request_many(
            self,
            operations: Iterable[OperationDef], token: Optional[Token]=None,
            timeout: Optional[float]=None,
            batch_size: int=100,
            max_retries: int=3,
            retry_delay:float=5,
    ) -> Iterable[Model]:
        """ Make multiple requests at once

        .. note::

            This is a blocking procedure
        """

        operations_list = list(operations)

        batches = (
            operations_list[start:start+batch_size]
            for start in range(0, len(operations_list), batch_size)
        )

        for batch in batches:

            retry = 0

            while True:
                try:
                    yield from tuple(self.request_many(batch, token, timeout))
                    break
                except EsiError as e:
                    retry += 1
                    if retry == max_retries:
                        raise e

                sleep(retry_delay)


