import pytest
from networkx import Graph


@pytest.fixture
def networkx_graph():
    graph = Graph()
    graph.graph['meta_a'] = 'ga1'
    graph.graph['meta_b'] = 'gb1'
    graph.add_node(1, meta_a='a1', meta_b='b1', meta_c='c1')
    graph.add_node(2, meta_a='a2', meta_b='b2')
    graph.add_node(3, meta_a='a3', meta_b='b3')
    graph.add_node(4, meta_a='a4', meta_b='b4')
    graph.add_edge(1, 2, meta_a='a1_2', meta_b='b1_2', meta_c='c1_2')
    graph.add_edge(1, 3, meta_a='a1_3', meta_b='b1_3')
    graph.add_edge(2, 3, meta_a='a2_3', meta_b='b2_3')
    return graph


