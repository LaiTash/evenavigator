import networkx
import pytest
import mock

from augmentedgraph.dictproxy import SimpleObservableDictProxy
from augmentedgraph.edgesmap import NeighborsMap
from . import EdgeTester


class TestNeighbors(EdgeTester):

    def get_neighbors_map(self, bunch=1):
        return NeighborsMap(bunch, self.graph, self.observer)

    def test_getitem(self):
        # Given an existing edge
        source, dest = self.create_edge(meta='value')

        neighbors_map = self.get_neighbors_map(source)

        # When accessing the edge with __getitem__
        # The correct meta must be returned
        assert neighbors_map.__getitem__(dest) == self.graph[source][dest]

    def test_getitem_returns_observable_proxy(self):
        # Must return an observable proxy
        source, dest = self.create_edge(meta='value')

        neighbors_map = self.get_neighbors_map(source)

        assert isinstance(
            neighbors_map.__getitem__(dest), SimpleObservableDictProxy
        )

    def test_meta_changes_notify_observer(self):
        """ All changes done to edge's meta must notify the observer """
        source, dest = self.create_edge(meta='value')
        neighbors_map = self.get_neighbors_map(source)

        neighbors_map[dest]['value'] = 'another value'

        self.observer._on_set_edge_meta.assert_called_with(
            source, dest, 'value', 'another value'
        )

    def test_meta_deletes_notify_observer(self):
        """ All changes done to edge's meta must notify the observer """
        source, dest = self.create_edge(meta='value')
        neighbors_map = self.get_neighbors_map(source)

        del neighbors_map[dest]['meta']

        self.observer._on_delete_edge_meta.assert_called_with(
            source, dest, 'meta'
        )


    def test_getitem_raise_keyerror(self):
        # Must raise KeyError if edge is not found
        source = self.create_node()

        neighbors_map = self.get_neighbors_map(source)
        with pytest.raises(KeyError):
            neighbors_map.__getitem__(-1)

    def test_len(self):
        a, b, c, d = self.create_multiple_nodes(4)
        self.graph.add_edge(a, b)
        self.graph.add_edge(a, c)
        self.graph.add_edge(b, c)

        neighbors_map = self.get_neighbors_map(a)

        assert len(neighbors_map) == 2

    def test_iter(self):
        a, b, c, d = self.create_multiple_nodes(4)
        self.graph.add_edge(a, b)
        self.graph.add_edge(a, c)
        self.graph.add_edge(b, c)
        self.graph.add_edge(b, d)
        neighbors_map = self.get_neighbors_map(a)

        assert set(neighbors_map) == {b, c}

    def test_contains(self):
        a, b, c, d = self.create_multiple_nodes(4)
        self.graph.add_edge(a, b)
        neighbors_map = self.get_neighbors_map(a)

        assert b in neighbors_map
        assert c not in neighbors_map