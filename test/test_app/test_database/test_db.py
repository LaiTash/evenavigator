from pony.orm import flush, db_session


def test_create_starsystem(db):
    with db_session:
        db.Region(
            id=0, name='Milky Way'
        )

        db.Constellation(
            id=0, name='No constellation', region=0
        )

        sol = db.StarSystem(
            id=0, name='Sol', security_status=0, constellation=0
        )

    with db_session:
        sol = db.StarSystem.get(name='Sol')
        assert sol.constellation.region.name == 'Milky Way'


def test_create_stargate(db):
    with db_session:
        db.Region(
            id=0, name='Milky Way'
        )

        db.Constellation(
            id=0, name='No constellation', region=0
        )

        db.Constellation(
            id=1, name='Cen', region=0
        )

        sol = db.StarSystem(
            id=0, name='Sol', security_status=0, constellation=0
        )
        alpha_centauri = db.StarSystem(
            id=1, name='Alpha Centauri', security_status=-1, constellation=1
        )
        sol_stargate = db.Stargate(
            id=0, star_system=sol, type_id=0,
            name='Stargate to Alpha Centauri',
        )
        alpha_centauri_stargate = db.Stargate(
            id=1, star_system=alpha_centauri, type_id=0,
            name='Stargate to Sol',
        )
        flush()

        sol_stargate.destination_stargate=1
        alpha_centauri_stargate.destination_stargate=0

    with db_session:
        sol_stargate = db.Stargate.get(id=0)
        assert sol_stargate.destination_stargate.name == "Stargate to Sol"

