from types import SimpleNamespace

from pony.orm import db_session

from app.database.starsystems import StarSystems


def test_insert_from_esi(db):
    # Needs an existing region and constellation
    with db_session:
        db.Region(id=0, name='0')
        db.Constellation(id=0, name='0', region=0)

    fake_data = dict(
        system_id=0, constellation_id=0, security_status=0, name='0'
    )

    StarSystems(db).insert_from_esi(fake_data)

    with db_session:
        assert db.StarSystem.get(id=0).constellation.region.name=='0'
