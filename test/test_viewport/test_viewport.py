import numpy as np
import pytest
from viewport.viewport import Viewport, _translation_matrix, _scale_matrix


@pytest.mark.parametrize(
    'direction,result', [
        ([1, 1], [[1, 0, 1], [0, 1, 1], [0, 0, 1]]),
        ([2, 1], [[1, 0, 2], [0, 1, 1], [0, 0, 1]]),
        ([2, 2], [[1, 0, 2], [0, 1, 2], [0, 0, 1]]),

    ]
)
def test_translation_matrix(direction, result):
    direction = np.array(direction)
    result = np.array(result)
    assert np.allclose(_translation_matrix(direction), result)


@pytest.mark.parametrize(
    'factor,origin,expected', [
        (1, None, [[1, 0, 0], [0, 1, 0], [0, 0, 1]]),
        (2, None, [[2, 0, 0], [0, 2, 0], [0, 0, 1]]),
        (.5, None, [[.5, 0, 0], [0, .5, 0], [0, 0, 1]]),
        (1, [0.5, 0.2], [[1, 0, 0], [0, 1, 0], [0, 0, 1]]),
        (0.5, [0.5, 0.2], [[0.5, 0, 0.25], [0, 0.5, 0.1], [0, 0, 1]]),
        (0.2, [0.5, 0.2], [[0.2, 0, 0.4], [0, 0.2, 0.16], [0, 0, 1]]),
    ]
)
def test_scale_matrix(factor, origin, expected):
    if origin is not None:
        origin = np.array(origin)
    expected = np.array(expected)
    result = _scale_matrix(factor, origin)

    assert np.allclose(result, expected)


class _CTranslate(object):
    def __init__(self, x, y):
        self.x, self.y = x, y

    def apply(self, viewport):
        viewport.translate(self.x, self.y)


class _CScale(object):
    def __init__(self, factor, origin=None):
        self.factor, self.origin = factor, origin

    def apply(self, viewport):
        viewport.scale(self.factor, self.origin)


class _CAspectRatio(object):
    def __init__(self, ratio):
        self.ratio = ratio

    def apply(self, viewport):
        viewport.aspect_ratio = self.ratio


class TestViewport(object):
    def setup_method(self):
        self.viewport = Viewport()

    @pytest.mark.parametrize(
        'aspect_ratio,height', [
            (1, 1),
            (2, .5),
            (.5, 2)
        ]
    )
    def test_height(self, aspect_ratio, height):
        self.viewport.aspect_ratio = aspect_ratio
        assert self.viewport.height == pytest.approx(height, .01)

    @pytest.mark.parametrize(
        'aspect_ratio,diagonal', [
            (1, 1.414),
            (2, 1.118),
            (.5, 2.236)
        ]
    )
    def test_diagonal(self, aspect_ratio, diagonal):
        self.viewport.aspect_ratio = aspect_ratio
        assert self.viewport.diagonal == pytest.approx(diagonal, .01)

    @pytest.mark.parametrize(
        'commands,point,expected', [
            [
                [('scale', 2)],
                (.5, .5), (1, 1)
            ],
            [
                [('scale', .5)],
                (.5, .5), (.25, .25)
            ],
            [
                [('scale', 2, (.5, .5))],
                (.5, .5), (.5, .5)
            ],
            [
                [('translate', 10, 10)],
                (.5, .5), (10.5, 10.5)
            ],
            [
                [
                    ('translate', .2, .2),
                    ('scale', 2)
                ],
                (.2, .2), (.8, .8)
            ],
            [
                [
                    ('translate', .2, .2),
                    ('scale', 2, (.4, .4)),
                    ('translate', .1, 0)
                ],
                (.2, .2), (.5, .4)
            ],
            [
                [
                    ('translate', .2, .2),
                    ('scale', 2, (.4, .4)),
                    ('translate', .1, 0),
                    ('set_aspect_ratio', 2)
                ],
                (.2, .2), (.5, .8)
            ]
        ]
    )
    def test_layout_to_viewport(self, commands, point, expected):
        for command in commands:
            getattr(self.viewport, command[0])(*command[1:])

        result = self.viewport.layout_to_viewport(point)

        assert result == pytest.approx(expected)

        # Now invert and test for sanity

        result = self.viewport.viewport_to_layout(expected)

        assert result == pytest.approx(point)