from pony.orm import Database

from esi.operationdef import OperationDef
from esi.ownership.pony import PonyTokenOwnersContainer
from esi.token.pony import PonyOwnedTokensContainer


def operation_location(character_id=0):
    return OperationDef(
        'get_characters_character_id_location', character_id=character_id
    )


def operation_location_url(character_id=0):
    return 'http://esi.tech.ccp.is/latest/characters/{}/location/'.format(
        character_id
    )


def pony_tokens_container():
    db = Database()
    db.bind('sqlite', ':memory:')
    container = PonyOwnedTokensContainer(db)
    db.generate_mapping(create_tables=True)
    return container


def pony_owners_container():
    db =  Database()
    db.bind('sqlite', ':memory:')
    container = PonyTokenOwnersContainer(db)
    db.generate_mapping(create_tables=True)
    return container