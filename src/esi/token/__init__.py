from abc import ABCMeta, abstractmethod
from datetime import datetime
from types import SimpleNamespace
from typing import Optional, Mapping, Generic, TypeVar, Any


class Token(SimpleNamespace, metaclass=ABCMeta):
    expires_at_utc: datetime

    def __init__(self, expires_at_utc: datetime) -> None:
        self.expires_at_utc = expires_at_utc

    @abstractmethod
    def to_standard_json(self) -> dict:
        pass

    @property
    def expires_in_seconds(self) -> float:
        return self.expires_at_utc.timestamp() - datetime.utcnow().timestamp()


class OwnedToken(Token):
    """

    Represents a token object with refresh token

    """

    refresh_token: str

    def __init__(
            self,
            refresh_token: str,
            access_token: str,
            expires_at_utc: datetime,
            token_type: Optional[str] = None
    ) -> None:
        super().__init__(expires_at_utc)
        self.access_token = access_token
        self.refresh_token = refresh_token
        self.token_type = token_type

    def to_standard_json(self) -> dict:
        return {
            'access_token': self.access_token,
            'expires_in': self.expires_in_seconds,
            'refresh_token': self.refresh_token,
            'token_type': self.token_type
        }

    def __copy__(self) -> 'OwnedToken':
        return self.__class__(
            self.refresh_token, self.access_token, self.expires_at_utc,
            self.token_type
        )

    def __repr__(self) -> str:
        result = "<OwnedToken: refresh: {}, access: {}, expires: {}, type: {}>"
        return result.format(
            self.refresh_token, self.access_token, self.expires_at_utc,
            self.token_type
        )


TokenKey = TypeVar('TokenKey')
TokenType = TypeVar('TokenType', bound=Token)


class TokensContainer(
    Generic[TokenKey, TokenType], Mapping[TokenKey, TokenType]
):
    """

    Tokens container utilizing a Mapping interface, with additional
    :py:method:`add` and :py:method:`delete` methods for adding, updating,
    and deleting tokens.

    """

    @abstractmethod
    def add(self, value: TokenType) -> None:
        """ Add a new token or update an existing one """
        pass

    @abstractmethod
    def delete(self, key: TokenKey) -> None:
        """ Remove a token """
        pass

