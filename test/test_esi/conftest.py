import os

import pytest

from esipy import App
from esi.esifacade import SecuritySpecs
from esi.token import OwnedToken

from datetime import datetime, timedelta


@pytest.fixture
def token():
    return OwnedToken(
        'refresh', 'access', datetime.utcnow() + timedelta(hours=1)
    )


@pytest.fixture(scope='module')
def app():
    base = os.path.realpath(os.path.dirname(__file__))
    path = os.path.join(base, 'data/tranquility.json')
    return App.create('file:///{}'.format(path))


@pytest.fixture
def security_specs():
    client_id = 'id'
    secret_key='key'
    redirect_uri = 'https://localhost'
    scopes = ['esi-assets.read_assets.v1']

    specs = SecuritySpecs(
        client_id, secret_key, redirect_uri, scopes
    )
    return specs
