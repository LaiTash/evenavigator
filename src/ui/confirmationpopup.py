import os
from typing import Callable, Any

from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.popup import Popup


class ConfirmationPopup(Popup):
    on_ok = ObjectProperty()
    text = StringProperty('Are you sure?')

    def __init__(self, on_ok: Callable[[], None], **kwargs: Any) -> None:
        self.on_ok = on_ok
        super().__init__(**kwargs)



_path = os.path.dirname(os.path.realpath(__file__))
Builder.load_file(os.path.join(_path, 'confirmationpopup.kv'))