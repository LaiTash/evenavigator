import fractions
from typing import Tuple


Dimensions = Tuple[float, float]


def correct_scale_to_fit(rect: Dimensions, to_fit: Dimensions) -> float:
    to_fit_x, to_fit_y = to_fit
    size_x, size_y = rect

    to_fit_aspect = to_fit_x / to_fit_y
    size_aspect = size_x / size_y

    if to_fit_aspect > size_aspect:
        scale = to_fit_y / size_y
    else:
        scale = to_fit_x / size_x

    return scale


def fit(rect: Dimensions, to_fit: Dimensions) -> Dimensions:
    scale = correct_scale_to_fit(rect, to_fit)
    return rect[0] * scale, rect[1] * scale
