from datetime import datetime, timedelta
import pytest
from copy import copy, deepcopy


def test_owned_token_init(token):
    assert token.access_token == 'access'
    assert token.refresh_token == 'refresh'
    expected_expiry = (datetime.utcnow() + timedelta(hours=1)).timestamp()
    assert token.expires_at_utc.timestamp() == pytest.approx(expected_expiry,1)


def test_owned_token_expires_in(token):
    assert token.expires_in_seconds == pytest.approx(3600, 1)


def test_owned_token_to_standard_json(token):
    json_data = token.to_standard_json()

    assert json_data == {
        'access_token': 'access',
        'refresh_token': 'refresh',
        'expires_in': pytest.approx(3600, 1),
        'token_type': None
    }


def test_copy(token):
    copied = copy(token)
    assert copied is not token
    assert copied == token