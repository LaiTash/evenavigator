from types import SimpleNamespace

from pony.orm import db_session

from app.database.stargates import Stargates


def test_insert_from_esi(db):
    # Needs an existing region and constellation
    with db_session:
        db.Region(id=0, name='0')
        db.Constellation(id=0, name='0', region=0)
        db.StarSystem(id=0, name='0', constellation=0, security_status=0)

    fake_data = dict(
        system_id=0, stargate_id=0, name='0', type_id=0
    )

    Stargates(db).insert_from_esi(fake_data)

    with db_session:
        assert db.Stargate.get(id=0).star_system.constellation.region.name=='0'


def test_mass_insert_from_esi(db):
    with db_session:
        db.Region(id=0, name='0')
        db.Constellation(id=0, name='0', region=0)
        db.StarSystem(id=0, name='0', constellation=0, security_status=0)
        db.StarSystem(id=1, name='1', constellation=0, security_status=0)

    fake_data = [
        dict(
            system_id=0, stargate_id=0, name='0', type_id=0,
            destination=dict(stargate_id=1),
        ),
        dict(
            system_id=1, stargate_id=1, name='1', type_id=0,
            destination=dict(stargate_id=0),
        ),
    ]

    Stargates(db).mass_insert_from_esi(fake_data)

    with db_session:
        assert db.Stargate.get(id=0).star_system.constellation.region.name=='0'
        assert db.Stargate.get(id=0).destination_stargate.id == 1
