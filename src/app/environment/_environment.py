import os
from concurrent.futures import ThreadPoolExecutor
from contextlib import contextmanager
from typing import Optional, Any

from pony.orm import Database

from app.config_schema.root import RootSchema
from esi.ownership import TokenOwnersContainer
from esi.token import TokensContainer
from .folders import EnvironmentFolders
from .portraitmanager import CharacterPortraitCache
from app.database import bind_evenavigator_tables
from esi.esifacade import AsyncEsiFacade, SecuritySpecs
from esi.ownership.pony import PonyTokenOwnersContainer
from esi.token.pony import PonyOwnedTokensContainer
import esipy


__all__ = ['AppEnvironment', 'create_environment']


class AppEnvironment(object):
    folders: Optional[EnvironmentFolders]
    config: Optional[RootSchema]
    esi: Optional[AsyncEsiFacade]
    db: Optional[Database]
    tokens: Optional[TokensContainer]
    owners: Optional[TokenOwnersContainer]
    portraits_cache: Optional[CharacterPortraitCache]


def create_environment(data_folder: str, esi_consts: Any) -> AppEnvironment:
    environment = AppEnvironment()
    environment.folders = EnvironmentFolders(data_folder)
    environment.config = RootSchema.from_defaults()

    esi_security_specs = SecuritySpecs(
        client_id=esi_consts.CLIENT_ID,
        secret_key=esi_consts.SECRET_KEY,
        redirect_uri=esi_consts.REDIRECT_URI,
        scopes=esi_consts.SCOPE.split(' ')
    )
    esi_app = esipy.App.create(
        esi_consts.ESI_URI.format(
            version=environment.config.esi.version,
            datasource=environment.config.esi.datasource
        )
    )


    executor = ThreadPoolExecutor(environment.config.esi.concurrent_requests)
    environment.esi = AsyncEsiFacade(executor, esi_app, esi_security_specs)

    db_filename = os.path.join(environment.folders.data_folder,
                               environment.config.database_filename)
    db = Database()
    db.bind('sqlite', db_filename, create_db=True)

    bind_evenavigator_tables(db)
    environment.tokens = PonyOwnedTokensContainer(db)
    environment.owners = PonyTokenOwnersContainer(db)
    db.generate_mapping(create_tables=True)
    environment.db = db

    portraits_folder = os.path.join(
        environment.folders.data_folder,
        environment.config.character_portraits_cache_folder
    )
    os.makedirs(portraits_folder, exist_ok=True)
    environment.portraits_cache = CharacterPortraitCache(
        db, environment.esi, portraits_folder

    )

    return environment
