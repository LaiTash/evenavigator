from typing import Tuple

Coord = Tuple[float, float]
Size = Tuple[float, float]
RGBA = Tuple[float, float, float, float]