from kivy.properties import StringProperty, BoundedNumericProperty

from configwrapper.config import KeyAccessConfigContainer


class ESISchema(KeyAccessConfigContainer):
    version = StringProperty('latest')
    datasource = StringProperty(
        'tranquility', options=['tranquility', 'singularity']
    )
    concurrent_requests = BoundedNumericProperty(10, min=1, max=100)
