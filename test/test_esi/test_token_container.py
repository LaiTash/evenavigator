from copy import copy
from datetime import datetime

import pytest

from esi.token import OwnedToken
from test.test_esi import pony_tokens_container

parametrize = pytest.mark.parametrize(
    'container', [
        pony_tokens_container()
    ]
)


@pytest.fixture
def second_token():
    token = OwnedToken(
        'refresh_new', 'access_new', datetime.utcnow()
    )
    return token


@parametrize
def test_add_new(container, token):
    # After a new token is added
    container.add(token)

    # The container should actually contain it
    assert container[token.refresh_token] == token


@parametrize
def test_add_existing(container, token):
    # Given a token
    container.add(token)

    # When an altered version is added to the container
    altered_token = copy(token)
    altered_token.access_token = 'other'
    container.add(altered_token)

    # Should contain an altered version
    assert container[token.refresh_token] == altered_token


@parametrize
def test_raises_keyerror(container):
    with pytest.raises(KeyError):
        container.__getitem__('nothing')


@parametrize
def test_delete(container, token):
    # Given a token in the container
    container.add(token)

    # When it is deleted
    container.delete(token.refresh_token)

    # Should no longer be in the container
    assert token.refresh_token not in container


@parametrize
def test_delete_deletes_not_everything(container, token, second_token):
    # Given two values in a container
    container.add(token)
    container.add(second_token)

    # When one is deleted
    container.delete(token.refresh_token)

    # Other should stay
    assert second_token.refresh_token in container


@parametrize
def test_len(container, token, second_token):
    # Given two values in a container
    container.add(token)
    container.add(second_token)

    # len should be 2
    assert len(container) == 2


@parametrize
def test_iter(container, token, second_token):
    # Given two values in a container
    container.add(token)
    container.add(second_token)

    # When iterating
    iter_result = iter(container)

    # Result value should contain correct keys
    assert set(iter_result) == {token.refresh_token,second_token.refresh_token}


@parametrize
def test_stores_independent_objects(container, token):
    # Given a value in a container
    container.add(token)

    # It should be independent of the object used to initialize it
    token.access_key = 'changed'
    assert container[token.refresh_token].access_token == 'access'