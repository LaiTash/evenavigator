import mock
import networkx
import pytest

from augmentedgraph.edgesmap import EdgesMetaObserver, EdgesMap


def edges_map():
    class _Observer(EdgesMetaObserver):
        _on_set_edge_meta = mock.MagicMock()
        _on_delete_edge_meta = mock.MagicMock()
    return EdgesMap(networkx.Graph(), _Observer())


class EdgeTester(object):
    def setup_method(self):
        self.edges_map = edges_map()
        self.observer = self.edges_map._observer
        self.graph = self.edges_map._graph

    def create_node(self, id_=0):
        self.graph.add_node(id_)
        return id_

    def create_multiple_nodes(self, num=2):
        for i in range(num):
            yield self.create_node(i)

    def create_edge(self, **meta):
        source, dest = self.create_multiple_nodes()
        self.graph.add_edge(source, dest, **meta)
        return source, dest

