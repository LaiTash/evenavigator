import pytest
import mock

from augmentedgraph.dictproxy import SimpleObservableDictProxy


@pytest.fixture
def proxy():
    subject = {
        'a': 'b',
        'c': 'd',
        'e': 'f'
    }
    proxy = SimpleObservableDictProxy(
        subject,
        mock.MagicMock(),
        mock.MagicMock()
    )
    return proxy


def test_getitem(proxy):
    assert proxy['a'] == proxy._subject['a']

def test_len(proxy):
    assert len(proxy) == len(proxy._subject)

def test_iter(proxy):
    assert set(proxy) == set(proxy._subject)

def test_setitem_new(proxy):
    proxy['x'] = 'y'
    assert proxy._subject['x'] == 'y'

def test_setitem_existing(proxy):
    proxy['a'] = 'c'
    assert proxy._subject['a'] == 'c'

def test_setitem_new_notify(proxy):
    proxy['x'] = 'y'
    assert proxy._on_set.called_with('x', 'y')

def test_setitem_existing_notify(proxy):
    proxy['a'] = 'c'
    assert proxy._on_set.called_with('a', 'c')

def test_delitem(proxy):
    del proxy['a']
    assert 'a' not in proxy._subject

def test_delitem_notify(proxy):
    del proxy['a']
    assert proxy._on_del.called_with('a')
