import os

from kivy import Config
from kivy.app import App
from kivy.properties import StringProperty
from kivy.resources import resource_add_path

from app.environment import AppEnvironment


class EveNavigatorApp(App):
    active_token_key = StringProperty(allownone=True)  # type: str

    def __init__(self, environment: AppEnvironment) -> None:
        super().__init__()
        self.environment = environment

        kivy_config = os.path.join(
            self.environment.folders.config_folder, 'kivy.ini'
        )

        Config.read(kivy_config)

        dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))

        resource_add_path(os.path.join(dir, 'kivy_assets/assets'))
        resource_add_path(os.path.join(dir, 'kivy_assets/kv'))

    def build(self) -> None:
        self.load_kv('app.kv')