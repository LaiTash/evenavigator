from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Thread
from typing import Type, Optional, Callable
import re
from urllib.parse import parse_qs, urlparse

from esipy import EsiSecurity

from esi.ownership import TokenOwnersContainer, TokenOwner
from esi.token import TokensContainer, OwnedToken
from datetime import datetime


__all__ = [
    'create_registrator_server'
]


class SimpleCodeRegistratorHandler(BaseHTTPRequestHandler):
    response_string: str
    security: EsiSecurity
    owners_container: TokenOwnersContainer
    tokens_container: TokensContainer

    def do_GET(self):
        parsed_uri = urlparse(self.path)
        code = parse_qs(parsed_uri.query).get('code')

        if code is None:
            return

        self.finalize_registration(code[0])

        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(self.response_string.encode())

        # Shut the server down
        assassin = Thread(target=self.server.shutdown)
        assassin.setDaemon(True)
        assassin.start()

    def finalize_registration(self, code):
        self.security.auth(code)
        token = self.add_token()

        self.add_owner(token)

    def add_owner(self, token):
        ownership = self.security.verify()
        self.owners_container[token.refresh_token] = TokenOwner(
            character_id=ownership['CharacterID'],
            character_name=ownership['CharacterName'],
            owner_hash=ownership['CharacterOwnerHash']
        )

    def add_token(self):
        token = OwnedToken(
            refresh_token=self.security.refresh_token,
            access_token=self.security.access_token,
            expires_at_utc=datetime.utcfromtimestamp(
                self.security.token_expiry
            )
        )

        self.tokens_container.add(token)
        return token


class _RegistratorServer(HTTPServer):
    def __init__(self,
                 shutdown_callback: Optional[Callable[[], None]]=None,
                 *args, **kwargs) -> None:
        self._shutdown_callback = shutdown_callback
        super().__init__(*args, **kwargs)

    def shutdown(self):
        super().shutdown()
        if self._shutdown_callback is not None:
            self._shutdown_callback()


def create_registrator_server(
        security: EsiSecurity,
        tokens_container: TokensContainer,
        owners_container: TokenOwnersContainer,
        shutdown_callback=None,
        response_string: str = "Thank you"
) -> HTTPServer:

    port = decide_port_from_uri(security.redirect_uri)
    handler = create_registrator_handler(
        security, tokens_container, owners_container, response_string
    )
    server = _RegistratorServer(shutdown_callback, ('', port), handler)
    return server


def create_registrator_handler(
        security: EsiSecurity,
        tokens_container: TokensContainer[str, OwnedToken],
        owners_container: TokenOwnersContainer,
        response_string: str="Thank you"
) -> Type:
    security = security
    tokens_container = tokens_container
    owners_container = owners_container
    response_string = response_string

    return type(
        '_Result', (SimpleCodeRegistratorHandler,), {
            'security': security,
            'tokens_container': tokens_container,
            'owners_container': owners_container,
            'response_string': response_string
        }
    )


def decide_port_from_uri(redirect_uri: str) -> int:
    url_parse_result = urlparse(redirect_uri)
    scheme = url_parse_result.scheme
    netloc = url_parse_result.netloc
    port = re.search(':(\d+)$', netloc)

    if port:
        return int(port.groups()[0])

    return {'http': 80, 'https': 443}[scheme]

