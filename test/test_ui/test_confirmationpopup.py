import mock

from ui.confirmationpopup import ConfirmationPopup

def test_confirmationpopup_on_ok():
    on_ok = mock.MagicMock()
    popup = ConfirmationPopup(on_ok)
    popup.dismiss = mock.MagicMock()

    popup.ok_button.trigger_action()

    # Should call the callback
    assert on_ok.called
    # Should close the popup
    assert popup.dismiss.called


def test_confirmationpopup_on_cancel():
    on_ok = mock.MagicMock()
    popup = ConfirmationPopup(on_ok)
    popup.dismiss = mock.MagicMock()

    popup.cancel_button.trigger_action()

    # Should NOT call the callback
    assert not on_ok.called
    # Should close the popup
    assert popup.dismiss.called