from datetime import datetime
from typing import Any

from pony.orm import Required, Optional, PrimaryKey, Set
from pony.orm import unicode


def bind_evenavigator_tables(db: Any) -> None:
    Entity = db.Entity  # type: Any

    class Portrait(Entity):
        _table_ = 'portraits'

        character_id = Required(int)
        key = Required(str)
        PrimaryKey(character_id, key)

        url = Required(unicode)

    class StarSystem(Entity):
        _table_ = 'star_systems'
        id = PrimaryKey(int)
        name = Required(unicode)
        security_status = Required(float)
        constellation = Required('Constellation')
        stargates = Set('Stargate')
        signatures = Set('Signature')
        system_kills = Optional('SystemKills')
        system_jumps = Optional('SystemJumps')

    class Constellation(Entity):
        _table_ = 'constellations'
        id = PrimaryKey(int)
        name = Required(unicode)
        star_systems = Set(StarSystem)
        region = Required('Region')

    class Region(Entity):
        _table_ = 'regions'
        id = PrimaryKey(int)
        name = Required(unicode)
        description = Optional(unicode, nullable=True)
        constellations = Set(Constellation)

    class Stargate(Entity):
        _table_ = 'stargates'
        id = PrimaryKey(int)
        type_id = Required(int)
        name = Required(unicode)
        destination_stargate = Optional('Stargate', reverse='source_stargates')
        source_stargates = Set('Stargate', reverse='destination_stargate',
                               cascade_delete=True)
        star_system = Required(StarSystem)

    class SystemJumps(Entity):
        _table_ = 'system_jumps'
        num = Required(int)
        star_system = PrimaryKey(StarSystem)

    class SystemKills(Entity):
        _table_ = 'system_kills'
        star_system = PrimaryKey(StarSystem)
        npc = Required(int)
        pod = Required(int)
        ship = Required(int)

    class Signature(Entity):
        signature_id = Required(str)
        star_system = Required(StarSystem)
        is_anomaly = Required(bool)
        group = Required(str)
        name = Optional(str, nullable=True)
        signal = Required(float, default=0)
        created_date = Required(datetime)
        PrimaryKey(signature_id, star_system)