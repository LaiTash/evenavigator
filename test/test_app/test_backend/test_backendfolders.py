import os
import pytest


@pytest.fixture
def abs_base_folder(base_folder):
    return os.path.abspath(os.path.expanduser(base_folder))


def test_base_folder(folders_object, abs_base_folder):
    result = folders_object.base_folder
    assert result == abs_base_folder


def test_config_folder(folders_object, abs_base_folder):
    result = folders_object.config_folder
    expected = os.path.join(abs_base_folder, 'config')
    assert result == expected


def test_data_folder(folders_object, abs_base_folder):
    result = folders_object.data_folder
    expected = os.path.join(abs_base_folder, 'data')
    assert result == expected


def test_creates_folders(folders_object):
    assert os.path.exists(folders_object.data_folder)
    assert os.path.exists(folders_object.base_folder)
