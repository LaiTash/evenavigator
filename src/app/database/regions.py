from pony.orm import db_session

from app.database.entity import EsiMassInsertableEntity


class Regions(EsiMassInsertableEntity):
    _entity_name = 'Region'

    @db_session
    def insert_from_esi(self, data: dict) -> None:
        self.entity(
            id=data['region_id'],
            name=data['name'],
            description=data.get('description')
        )
