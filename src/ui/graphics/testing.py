""" Unit-testing class """
import os
from array import array
from contextlib import contextmanager
from functools import reduce
from operator import add

import pytest
from kivy.graphics.context_instructions import Scale
from kivy.graphics.fbo import Fbo
from kivy.core.image import Image
from PIL import Image
from kivy.graphics.texture import Texture
from kivy.uix.widget import Widget

BUILD_RERERENCE_IMAGES_ENVVAR = 'BUILD_REFERENCE_IMAGES'


def _pil_compare_images(img1: Image, img2: Image) -> float:
    hist_1 = img1.histogram()
    hist_2 = img2.histogram()
    rms = reduce(add, map(lambda a, b: (a-b)**2, hist_1, hist_2))/ len(hist_1)
    rms **= 0.5
    return rms


def compare_image_files(first_filename: str, second_filename: str) -> float:
    first_image = Image.open(first_filename)
    second_image = Image.open(second_filename)
    return _pil_compare_images(first_image, second_image)


@contextmanager
def render_to_texture():
    fbo = Fbo()
    with fbo:
        yield fbo.texture
    fbo.draw()

