from types import SimpleNamespace

from app.database.entity import *


class DummyEntity(Entity):
    _entity_name = 'Dummy'


class DummyInsertableEntity(DummyEntity, EsiInsertableEntity):
    @db_session
    def insert_from_esi(self, data):
        self.entity(
            id=data['id'], name=data['name']
        )


class DummyMassInsertableEntity(
    DummyInsertableEntity, EsiMassInsertableEntity
):
    pass


class TestEntity:
    def test_getitem(self, db):
        with db_session:
            db.Dummy(id=0, name='0')
            db.Dummy(id=1, name='1')

        assert DummyEntity(db)[0].name == '0'

    def test_delete_all(self, db):
        with db_session:
            db.Dummy(id=0, name='0')
            db.Dummy(id=1, name='1')

        DummyEntity(db).delete_all()

        with db_session:
            assert len(db.Dummy.select()) == 0


class TestInsertableEntity:
    def test_insert_from_esi(self, db):
        faked_esi = dict(id=0, name='0')

        DummyInsertableEntity(db).insert_from_esi(faked_esi)

        with db_session:
            assert db.Dummy.get(id=0).name == '0'


class TestMassInsertableEntity:
    def test_mass_insert_from_esi(self, db):
        faked_esi_data = [
            dict(id=0, name='0'),
            dict(id=1, name='1')
        ]
        DummyMassInsertableEntity(db).mass_insert_from_esi(faked_esi_data)

        with db_session():
            dummies = list(db.Dummy.select())

        assert dummies[0].name == '0'
        assert dummies[1].name == '1'


    def test_recreate_from_esi(self, db):
        with db_session:
            db.Dummy(id=2, name='0')
            db.Dummy(id=3, name='1')

        faked_esi_data = [
            dict(id=0, name='0'),
            dict(id=1, name='1')
        ]

        DummyMassInsertableEntity(db).recreate_from_esi(faked_esi_data)

        with db_session():
            dummies = list(db.Dummy.select())

        assert len(dummies) == 2

        assert dummies[0].name == '0'
        assert dummies[1].name == '1'
