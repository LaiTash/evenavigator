from typing import Tuple, Any

from kivy.graphics.context_instructions import PushMatrix, PopMatrix
from kivy.graphics.context_instructions import Translate, Scale
from kivy.properties import NumericProperty, ObjectProperty
from kivy.uix.widget import Widget


class NodeWidget(Widget):
    node_size = NumericProperty()  # type: float

    _translate = ObjectProperty()
    _scale = ObjectProperty()

    def __init__(self, **kwargs:Any) -> None:
        super().__init__(**kwargs)

    @property
    def aspect_ratio(self) -> float:
        canvas_width, canvas_height = self.canvas_size
        return canvas_width / canvas_height

    def update_graphics(self) -> None:
        self._before_draw()

        with self.canvas:
            PushMatrix()
            self._translate = Translate(0, 0)
            self._scale = Scale(0, 0)
            self.canvas_size = self._draw()
            PopMatrix()

        self._update_translation()
        self._update_scale()


    def on_node_size(self, instance: Any, value: Tuple[float, float]) -> None:
        self.width = self.node_size
        self.height = self.node_size / self.aspect_ratio

    def on_pos(self, instance: Any, value: Tuple[float, float]) -> None:
        self._update_translation()

    def _update_translation(self) -> None:
        if self._translate is not None:
            self._translate.x, self._translate.y = self._get_translation()

    def on_size(self, instance: Any, value: Tuple[float, float]) -> None:
        self._update_scale()

    def _update_scale(self) -> None:
        if self._scale is not None:
            self._scale.x, self._scale.y = self._get_scale()

    def _get_translation(self) -> Tuple[float, float]:
        return self.x, self.y

    def _get_scale(self) -> Tuple[float, float]:
        canvas_width, canvas_height = self.canvas_size
        return self.width / canvas_width, self.height / canvas_height

    def _draw(self) -> Tuple[float, float]:
        """

        :return: resulting draw size
        """
        return 0, 0

