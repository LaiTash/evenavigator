from esi.operationdef import OperationDef, bind_operation_def
from . import operation_location, operation_location_url


def test_operation_create():
    # Given some operation definition values
    operation_id = 'some_operation'
    operation_args = (1, 2)
    operation_kwargs = {'a': 'b', 'c': 'd'}

    # When a new operation is defined with those values
    operation = OperationDef(operation_id, *operation_args, **operation_kwargs)

    # It contents should reflect those values
    assert operation.id == operation_id
    assert operation.args == operation_args
    assert operation.kwargs == operation_kwargs


def test_bind_operation_def(app):
    request = bind_operation_def(app, operation_location(0))[0]
    request.prepare()

    assert(request.url == operation_location_url(0))
