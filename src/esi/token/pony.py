""" Pony ORM containers """
from abc import abstractmethod
from typing import Any, Iterator

from pony.orm import Required, unicode, Optional, db_session, Database, \
    select, PrimaryKey

from . import TokensContainer, OwnedToken
from datetime import datetime


class PonyOwnedTokensContainer(TokensContainer[str, OwnedToken]):
    """

    TokensContainer utilizing pony orm database object.

    """

    def __init__(self, db: Database) -> None:
        self.__db = db
        self.__define_entities()

    @db_session
    def add(self, token: OwnedToken) -> None:
        if token.refresh_token in self:
            self.__change(token)
        else:
            self.__add(token)

    @db_session
    def delete(self, key: str) -> None:
        self.__get(key).delete()

    @db_session
    def __getitem__(self, key: str) -> OwnedToken:
        return self.__to_token(self.__get(key))

    @db_session
    def __contains__(self, key: str) -> bool:  # type: ignore
        return self.__db.Token.exists(refresh_token=key)

    @db_session
    def __len__(self) -> int:
        return len(self.__db.Token.select())

    @db_session
    def __iter__(self) -> Iterator[str]:
        return iter(select(token.refresh_token for token in self.__db.Token))

    def __define_entities(self) -> None:
        Entity = self.__db.Entity  # type: Any
        class Token(Entity):
            refresh_token = PrimaryKey(unicode)
            access_token = Required(unicode)
            expires_at_utc = Required(datetime)
            token_type = Optional(unicode, nullable=True)

    def __to_token(self, token: Any) -> OwnedToken:
        return OwnedToken(
            token.refresh_token,
            token.access_token,
            token.expires_at_utc,
            token.token_type
        )

    def __get(self, key: str) -> Any:
        result = self.__db.Token.get(refresh_token=key)

        if result is None:
            raise KeyError(key)

        return result

    def __change(self, token: OwnedToken) -> None:
        self.__db.Token.get(refresh_token=token.refresh_token).set(
            access_token=token.access_token,
            expires_at_utc=token.expires_at_utc,
            token_type=token.token_type
        )

    def __add(self, token: OwnedToken) -> None:
        self.__db.Token(
            refresh_token=token.refresh_token,
            access_token=token.access_token,
            expires_at_utc=token.expires_at_utc,
            token_type=token.token_type
        )


