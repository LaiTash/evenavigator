from traceback import *

from io import StringIO

from simpletools.exception import *
import pytest


def test_raise_exception():
    with pytest.raises(KeyError):
        raise_exception(KeyError())


def test_format_exception_traceback():
    try:
        1/0
    except ZeroDivisionError as e:
        formatted_tb = format_exc()
        assert format_exception_traceback(e) == formatted_tb


def test_log_exception_handler():
    stream = StringIO()
    handler = LogExceptionHandler(stream.write, str)

    handler(Exception("all fine"))

    assert stream.getvalue() == 'all fine'