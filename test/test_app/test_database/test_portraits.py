""" :py:class:`app.database.portraits.Portraits` tests """
import os

from datetime import datetime, timedelta

import pytest
from pony.orm import db_session

from app.database.portraits import Portraits


def test_get_keys_for_character(db):
    """ Test case for :py:meth:`Portraits.get_keys_for_character` """
    portraits = Portraits(db)

    character_0_data = {
        "px128x128":
            "https://imageserver.eveonline.com/Character/95465499_128.jpg",
        "px256x256":
            "https://imageserver.eveonline.com/Character/95465499_256.jpg",
    }
    portraits.update_from_esi(0, character_0_data)

    character_1_data = {
        "px512x512":
            "https://imageserver.eveonline.com/Character/95465499_512.jpg",
        "px64x64":
            "https://imageserver.eveonline.com/Character/95465499_64.jpg"
    }

    portraits.update_from_esi(1, character_1_data)

    assert portraits.character_keys(0) == set(character_0_data.keys())


def test_update_from_esi_new(db):
    """ Test case for :py:meth:`Portraits.update_from_esi`

    Case: use :py:meth:`Portraits.update_from_esi` to insert new
    data from swagger model as dictionary

    :param db: test database
    """
    portraits = Portraits(db)
    data = {
        "px128x128":
            "https://imageserver.eveonline.com/Character/95465499_128.jpg",
        "px256x256":
            "https://imageserver.eveonline.com/Character/95465499_256.jpg",
        "px512x512":
            "https://imageserver.eveonline.com/Character/95465499_512.jpg",
        "px64x64":
            "https://imageserver.eveonline.com/Character/95465499_64.jpg"
    }
    portraits.update_from_esi(0, data)

    for key, url in data.items():
        portrait_record = Portraits(db).get(character_id=0, key=key)
        assert portrait_record.url == url


def test_update_from_esi_update(db):
    """ Test case for :py:meth:`Portraits.update_from_esi`

    Case: use update_from_esi to update existing records

    :param db: test database
    """

    # Populate "existing" values
    portraits = Portraits(db)
    existing_data = {
        "px128x128":
            "https://imageserver.eveonline.com/Character/95465499_128.jpg",
        "px256x256":
            "https://imageserver.eveonline.com/Character/95465499_256.jpg",
        "px64x64":
            "https://imageserver.eveonline.com/Character/95465499_64.jpg"
    }
    portraits.update_from_esi(0, existing_data)

    new_data = {
        "px128x128":
            "https://imageserver.eveonline.com/Character/95465499_128.png",
        "px256x256":
            "https://imageserver.eveonline.com/Character/95465499_256.jpg",
        "px512x512":
            "https://imageserver.eveonline.com/Character/95465499_512.jpg",
    }

    portraits.update_from_esi(0, new_data)

    assert portraits.character_keys(0) == set(new_data)

    for key, url in new_data.items():
        portrait_record = Portraits(db).get(character_id=0, key=key)
        assert portrait_record.url == url
