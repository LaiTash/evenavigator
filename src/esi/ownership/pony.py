from typing import Any, Iterator

from pony.orm import Required, unicode, Optional, db_session, Database, \
    select, PrimaryKey

from . import TokenOwnersContainer, TokenOwner
from datetime import datetime


class PonyTokenOwnersContainer(TokenOwnersContainer):
    """

    TokenOwnersContainer utilizing a pony orm database object.

    """

    def __init__(self, db: Database) -> None:
        self.__db = db
        self.__define_entities()

    def __define_entities(self) -> None:
        Entity = self.__db.Entity  # type: Any
        class TokenOwner(Entity):
            refresh_token = PrimaryKey(unicode)
            character_id = Required(int)
            character_name = Required(unicode)
            owner_hash = Required(unicode)

    def __from_stored(self, owner: Any) -> TokenOwner:
        return TokenOwner(
            owner.character_id,
            owner.character_name,
            owner.owner_hash
        )

    def __get(self, key: str) -> Any:
        result = self.__db.TokenOwner.get(refresh_token=key)

        if result is None:
            raise KeyError(key)

        return result

    def __change(self, key: str, value: TokenOwner) -> None:
        self.__db.TokenOwner.get(refresh_token=key).set(
            character_id=value.character_id,
            character_name=value.character_name,
            owner_hash=value.owner_hash
        )

    def __add(self, key: str, value: TokenOwner) -> None:
        self.__db.TokenOwner(
            refresh_token=key,
            character_id=value.character_id,
            character_name=value.character_name,
            owner_hash=value.owner_hash
        )

    @db_session
    def __setitem__(self, key: str, value: TokenOwner) -> None:
        if key in self:
            self.__change(key, value)
        else:
            self.__add(key, value)

    @db_session
    def __getitem__(self, key: str) -> TokenOwner:
        return self.__from_stored(self.__get(key))

    @db_session
    def __contains__(self, key: str) -> bool:  # type: ignore
        return self.__db.TokenOwner.exists(refresh_token=key)

    @db_session
    def __len__(self) -> int:
        return len(self.__db.TokenOwner.select())

    @db_session
    def __iter__(self) -> Iterator[str]:
        return iter(
            select(token.refresh_token for token in self.__db.TokenOwner)
        )

    @db_session
    def __delitem__(self, key: str) -> None:
        self.__get(key).delete()
