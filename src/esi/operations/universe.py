from typing import Optional

from ..operationdef import OperationDef


def systems(id: Optional[int]=None) -> OperationDef:
    if id is None:
        return OperationDef('get_universe_systems')
    return OperationDef('get_universe_systems_system_id', system_id=id)


def stargates(id: int) -> OperationDef:
    return OperationDef('get_universe_stargates_stargate_id', stargate_id=id)


def regions(id: Optional[int]=None) -> OperationDef:
    if id is None:
        return OperationDef('get_universe_regions')
    return OperationDef('get_universe_regions_region_id', region_id=id)


def constellations(id: Optional[int]=None) -> OperationDef:
    if id is None:
        return OperationDef('get_universe_constellations')
    return OperationDef(
        'get_universe_constellations_constellation_id', constellation_id=id
    )


def jumps() -> OperationDef:
    return OperationDef('get_universe_system_jumps')


def kills() -> OperationDef:
    return OperationDef('get_universe_system_kills')
