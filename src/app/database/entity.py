from abc import ABCMeta, abstractmethod
from typing import Any, List, Iterable

from pony.orm import db_session, Database
from pyswagger.primitives import Model

from esi.esifacade import EsiFacade


class Entity(object):
    _entity_name: str

    def __init__(self, db: Database) -> None:
        self.__db = db

    @property
    def db(self) -> Database:
        return self.__db

    @property
    def entity(self) -> Any:
        return getattr(self.__db, self._entity_name)

    @db_session
    def __getitem__(self, item):
        return self.entity.__getitem__(item)

    @db_session
    def get(self, **kwargs:Any) -> Any:
        return self.entity.get(**kwargs)

    @db_session
    def delete_all(self) -> None:
        self.entity.select().delete()


class EsiInsertableEntity(Entity, metaclass=ABCMeta):
    @abstractmethod
    def insert_from_esi(self, data: dict) -> None:
        raise NotImplementedError()


class EsiMassInsertableEntity(EsiInsertableEntity):
    def recreate_from_esi(self, data: Iterable[dict]) -> None:
        self.delete_all()
        self.mass_insert_from_esi(data)

    @db_session
    def mass_insert_from_esi(self, data: Iterable[dict]) -> None:
        for starsystem_data in data:
            self.insert_from_esi(starsystem_data)

