from functools import partial
from typing import Mapping, Any, Iterable, Iterator

from networkx import Graph

from augmentedgraph.dictproxy import SimpleObservableDictProxy

NodeId = Any


class NodesMetaObserver(object):
    def _on_set_node_meta(
            self, node_id: NodeId, key: Any, value: Any
    ) -> None:  # pragma: no cover
        pass

    def _on_delete_node_meta(
            self, node_id:NodeId, key: Any
    ) -> None:  # pragma: no cover
        pass


class NodesMapping(Mapping[NodeId, SimpleObservableDictProxy]):
    def __init__(self, graph: Graph, observer: NodesMetaObserver) -> None:
        self._graph = graph
        self._observer = observer

    def __getitem__(self, node: NodeId) -> SimpleObservableDictProxy:
        subject = self._graph.node[node]
        on_set = partial(self._observer._on_set_node_meta, node)
        on_delete = partial(self._observer._on_delete_node_meta, node)
        return SimpleObservableDictProxy(subject, on_set, on_delete)

    def __len__(self) -> int:
        return self._graph.number_of_nodes()

    def __iter__(self) -> Iterator[NodeId]:
        return self._graph.nodes_iter()

    def __contains__(self, node_id: NodeId) -> bool:
        return self._graph.has_node(node_id)