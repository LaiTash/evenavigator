from types import SimpleNamespace

from pony.orm import db_session

from app.database.regions import Regions


def test_insert_from_esi(db):
    fake_data = dict(
        region_id=0, name='0', get=lambda key: None
    )

    Regions(db).insert_from_esi(fake_data)


    with db_session:
        assert db.Region.get(id=0).name=='0'
