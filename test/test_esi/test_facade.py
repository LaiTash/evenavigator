import os
from concurrent.futures import ThreadPoolExecutor
from types import SimpleNamespace
from warnings import warn

import pytest
from esipy import EsiClient

from esi.esifacade import EsiFacade, EsiError, AsyncEsiFacade
from . import operation_location, operation_location_url
from esipy.cache import FileCache


CLIENT_RESPONSES = {
    operation_location_url(0): {
        'status': 200,
        'data': {'ok': True}
    },
    operation_location_url(1): {
        'status': 404,
        'data': {'error': 'Not found'}
    },
    operation_location_url(2): {
        'status': 200,
        'data': {'id': 0}
    },
    operation_location_url(3): {
        'status': 200,
        'data': {'id': 1}
    },

}


class DummyClient(EsiClient):
    def _request(self, req_and_resp, raw_body_only=None, opt=None):
        # Just return the arguments
        request = req_and_resp[0]
        request.prepare()

        response = {
            'status': 200,
            'data': {
                'url': request.url,
                'client': self,
            }
        }

        return SimpleNamespace(**response)


@pytest.fixture
def facade(app, security_specs):
    class DummyEsiFacade(AsyncEsiFacade):
        requests_made = -1
        def create_client(self, token=None):
            return DummyClient(self.create_security(token))

        def _request(self, operation_def, token=None):
            self.requests_made += 1
            if self.requests_made == 100:
                raise EsiError(SimpleNamespace(data={'error': 'oops'}))

            return super()._request(operation_def, token)

    executor = ThreadPoolExecutor(10)
    return DummyEsiFacade(executor, app, security_specs)


def test_get_auth_uri(facade):
    assert facade.get_auth_uri() == "https://login.eveonline.com/oauth/" \
                                    "authorize?response_type=code" \
                                    "&redirect_uri=https%3A%2F%2Flocalhost" \
                                    "&client_id=id" \
                                    "&scope=esi-assets.read_assets.v1"


def test_create_client_without_token(app, security_specs):
    client = EsiFacade(app, security_specs).create_client()
    assert client.security.access_token is None


def test_create_client_with_token(app, security_specs, token):
    client = EsiFacade(app, security_specs).create_client(token)
    assert client.security.access_token == 'access'


def test_request_ok(facade):
    result = facade.request(operation_location(0))
    expected_url = operation_location_url(0)
    assert result['url'] == expected_url


def test_request_fail(facade):
    facade.requests_made = 99  # 100 is error
    with pytest.raises(EsiError):
        facade.request(operation_location(1))


def test_request_with_token(facade, token):
    result = facade.request(operation_location(0), token)
    security = result['client'].security

    assert security.access_token == 'access'


def test_async_request(facade):
    future = facade.async_request(operation_location(0))
    while not future.done(): pass

    assert future.result()['url'] == operation_location_url(0)


def test_async_request_with_token(facade, token):
    future = facade.async_request(operation_location(0), token)
    while not future.done(): pass

    assert future.result()['client'].security.access_token == 'access'


def test_request_many(facade):
    args = list(range(10))
    result = facade.request_many(map(operation_location, args))
    result_urls = set(item['url'] for item in result)
    expected_urls = set(map(operation_location_url, args))

    assert result_urls == expected_urls


def test_request_many_with_token(facade, token):
    result = facade.request_many(map(operation_location, (2, 3)), token)
    access_tokens = (item['client'].security.access_token for item in result)

    assert set(access_tokens) == {'access'}


def test_carefully_request_many(facade):
    args = list(range(200))
    operations = map(operation_location, args)
    results = facade.carefully_request_many(
        operations, batch_size=30, retry_delay=0
    )

    result_urls_list = list(item['url'] for item in results)
    result_urls = set(result_urls_list)

    assert len(result_urls_list) == len(result_urls)

    expected_urls = set(map(operation_location_url, args))

    assert result_urls == expected_urls


def test_carefully_request_many_with_token(facade, token):
    result = facade.carefully_request_many(
        map(operation_location, (2, 3)), token
    )
    access_tokens = (item['client'].security.access_token for item in result)

    assert set(access_tokens) == {'access'}

