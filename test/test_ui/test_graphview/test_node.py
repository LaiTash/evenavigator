from kivy.graphics.context_instructions import Scale, Translate

from ui.graphview.node import NodeWidget


class DummyNode(NodeWidget):
    def _before_draw(self):
        pass

    def _draw(self):
        pass

    @property
    def canvas_size(self):
        return (100, 50)


def test_on_node_size():
    node = DummyNode()
    node._scale = Scale(0, 0)

    node.node_size = 200
    assert node.width == 200
    assert node.height == 100
    assert node._scale.x == 2
    assert node._scale.y == 2


def test_on_node_pos():
    node = DummyNode()
    node._translate = Translate(0, 0)

    node.pos = 10, 10

    assert node._translate.x == 10
    assert node._translate.y == 10


def test_on_size():
    node = DummyNode()
    node._scale = Scale(0, 0)

    node.node_size = 100

    node.size = (200, 200)

    assert node._scale.x == 2
    assert node._scale.y == 4
