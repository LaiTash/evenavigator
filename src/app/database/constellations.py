from pony.orm import db_session

from app.database.entity import EsiMassInsertableEntity


class Constellations(EsiMassInsertableEntity):
    _entity_name = 'Constellation'

    @db_session
    def insert_from_esi(self, data: dict) -> None:
        self.entity(
            id=data['constellation_id'],
            name=data['name'],
            region=data['region_id']
        )
