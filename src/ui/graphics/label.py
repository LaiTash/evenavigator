from typing import Any

from kivy.graphics.context_instructions import Color
from kivy.graphics.instructions import InstructionGroup
from kivy.graphics.texture import Texture
from kivy.graphics.vertex_instructions import Rectangle

from .tools import fit
from ._types import Coord, Size
from kivy.core.text import Label as CoreLabel


class Label(InstructionGroup):
    """

    An instruction group that draws a text inside a specified rectangular area,
     positioned in the middle of it.

    """
    def __init__(
            self, text: str, pos: Coord, size: Size,
            text_area_scale: float=1, **label_args: Any
    ) -> None:
        self._text = text
        self._pos = pos
        self._size = size
        self._text_area_scale = text_area_scale
        self._label_args = label_args
        super().__init__()
        self.rebuild()

    def rebuild(self) -> None:
        self.clear()

        text_width = self.width * self.text_area_scale
        text_height = self.height * self.text_area_scale

        label_texture = self._create_label_texture()

        fit_size = fit(label_texture.size, (text_width, text_height))
        fit_width, fit_height = fit_size

        x = self.x + (self.width - fit_width) / 2
        y = self.y + (self.height - fit_height) / 2
        pos = (x, y)

        self.add(Color(1, 1, 1, 1))
        self.add(Rectangle(pos=pos, size=fit_size, texture=label_texture))

    def _create_label_texture(self) -> Texture:
        label = self._create_label()
        label.refresh()
        return label.texture

    def _create_label(self) -> CoreLabel:
        return CoreLabel(text=self.text, **self._label_args)

    @property
    def text(self) -> str:
        return self._text

    @property
    def pos(self) -> Coord:
        return self._pos

    @property
    def x(self) -> float:
        return self._pos[0]

    @property
    def y(self) -> float:
        return self._pos[1]

    @property
    def width(self) -> float:
        return self._size[0]

    @property
    def height(self) -> float:
        return self._size[1]

    @property
    def size(self) -> Coord:
        return self._pos

    @property
    def text_area_scale(self) -> float:
        return self._text_area_scale

    @property
    def label_args(self) -> dict:
        return self._label_args