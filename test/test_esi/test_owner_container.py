import pytest

from esi.ownership import TokenOwner
from . import pony_owners_container


@pytest.fixture()
def container():
    return pony_owners_container()


@pytest.fixture()
def owner():
    return TokenOwner(
        character_id=0,
        character_name='Joe',
        owner_hash='something'
    )


@pytest.fixture()
def second_owner():
    return TokenOwner(
        character_id=1,
        character_name='Lai',
        owner_hash='something'
    )


def test_add(container, owner):
    # After a new value is added to a container
    container['1'] = owner

    # The container should contain the said value
    assert container['1'] == owner


def test_raises_keyerror(container):
    with pytest.raises(KeyError):
        container.__getitem__('nothing')


def test_setitem_existing(container, owner, second_owner):
    # Given an existing value
    container['1'] = owner

    # If a new value is added with the same key
    container['1'] = second_owner

    # The container should contain the new value instead of the old one
    assert container['1'] == second_owner


def test_delitem(container, owner):
    # Given a value in a container
    container['1'] = owner

    # When a value is deleted
    del container['1']

    # It should no longer be in the container
    assert '1' not in container


def test_delitem_deletes_not_everything(container, owner, second_owner):
    # Given two values in a container
    container['1'] = owner
    container['2'] = second_owner

    # When one is deleted
    del container['1']

    # The other should stay
    assert '2' in container


def test_len(container, owner, second_owner):
    # If there are two values in a container
    container['1'] = owner
    container['2'] = second_owner

    # len should return 2
    assert len(container) == 2


def test_iter(container, owner, second_owner):
    # Given two values in a container
    container['1'] = owner
    container['2'] = second_owner

    # The result of iter should contain the correct keys
    iter_result = iter(container)
    assert set(iter_result) == {'1', '2'}


def test_stores_independent_objects(container, owner):
    # Given a value in a container
    container['1'] = owner

    # When an object used to construct that value changes
    owner.character_name = 'changed'

    # The stored value must not change
    assert container['1'].character_name == 'Joe'


def test_key_from_character_id(container, owner):
    container['1'] = owner
    assert container.key_from_character_id(0) == '1'


def test_key_from_character_id_faile(container, owner):
    container['1'] = owner
    with pytest.raises(LookupError):
        assert container.key_from_character_id(100)