import os
from pathlib import Path
from threading import Thread
from typing import Any, Iterable
from itertools import chain

from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from pony.orm import Database

from app.database.constellations import Constellations
from app.database.regions import Regions
from app.database.stargates import Stargates
from app.database.starsystems import StarSystems
from esi.esifacade import AsyncEsiFacade
from esi.operations.universe import regions, constellations, systems, stargates
from . import UsesEnvironment

__all__ = ['RebuildEveDataWidget']


class RebuildEveDataPopup(Popup, UsesEnvironment):
    state = StringProperty()
    progress_max = NumericProperty(0)
    progress = NumericProperty(0)


    def __init__(self, **kwargs:Any) -> None:
        super().__init__(**kwargs)
        self._esi = self.environment.esi
        self._db = self.environment.db

    def open(self, *largs):
        super().open(*largs)
        self.bind(on_open=lambda *_: self._start_rebuilding_eve_data())

    def _start_rebuilding_eve_data(self):
        Thread(target=self._rebuild_eve_data).start()

    def _rebuild_eve_data(self) -> None:
        self._rebuild_regions()
        self._rebuild_constellations()
        stargates_ids = self._rebuild_star_systems()
        self._rebuild_stargates(stargates_ids)
        self.dismiss()

    def _reset_progress(self, state: str, progress_max: int) -> None:
        self.state = state
        self.progress, self.progress_max = 0, progress_max

    def _iterate_update_progress(self, iterable: Iterable) -> Iterable:
        for item in iterable:
            self.progress += 1
            yield item

    def _rebuild_regions(self) -> None:
        self._reset_progress('Requesting regions indices', 1)
        region_ids = self._esi.request(regions())
        self.progress = 1

        self._reset_progress('Requesting regions data', len(region_ids))
        operations = map(regions, region_ids)
        Regions(self._db).recreate_from_esi(
            self._iterate_update_progress(
                self._esi.carefully_request_many(operations)
            )
        )

    def _rebuild_constellations(self) -> None:
        self._reset_progress('Requesting constellations indices', 1)
        constellations_ids = self._esi.request(constellations())
        self.progress = 1

        self._reset_progress('Requesting constellations data',
                             len(constellations_ids))

        operations = map(constellations, constellations_ids)
        Constellations(self._db).recreate_from_esi(
            self._iterate_update_progress(
                self._esi.carefully_request_many(operations)
            )
        )

    def _rebuild_star_systems(self) -> Iterable[int]:
        """ Rebuild star systems

         :return: stargate indices
        """
        self._reset_progress('Requesting star systems indices', 1)
        systems_ids = self._esi.request(systems())
        self.progress = 1

        self._reset_progress('Requesting star systems data', len(systems_ids))
        operations = map(systems, systems_ids)
        data = list(
            self._iterate_update_progress(
                self._esi.carefully_request_many(operations)
            )
        )
        StarSystems(self._db).mass_insert_from_esi(data)

        stargate_ids = chain(*map(lambda s: s.get('stargates', []), data))
        return stargate_ids


    def _rebuild_stargates(self, stargate_ids: Iterable[int]) -> None:
        stargate_ids_list = list(stargate_ids)
        self._reset_progress('Requesting stargates data',
                             len(stargate_ids_list))

        operations = map(stargates, stargate_ids_list)
        Stargates(self._db).mass_insert_from_esi(
            self._iterate_update_progress(
                self._esi.carefully_request_many(operations)
            )
        )


_path = Path(__file__).with_suffix('.kv')
Builder.load_file(str(_path))
